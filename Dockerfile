FROM ruby:3.1.3

# default DEPLOYABLE
# is null for local build and
# development
ARG DEPLOYABLE

ENV APP_PATH=/opt/app-root/src
ENV HOME=/opt/app-root

WORKDIR ${APP_PATH}

COPY Gemfile* ${APP_PATH}/

RUN curl -s https://gitlab.oit.duke.edu/oasis-application-development/developer-tools/-/raw/v1.5.1/ruby/polymath/polymath.sh | bash

COPY . ${APP_PATH}
RUN bundle install --retry 3 \
    && rm -rf `gem env gemdir`/cache/*.gem \
    && find `gem env gemdir`/gems/ -name "*.c" -delete \
    && find `gem env gemdir`/gems/ -name "*.o" -delete

RUN if [ -n "${DEPLOYABLE}" ]; then \
      RAILS_ENV=production rails assets:precompile && \
      rm -rf tmp/cache vendor/assets; fi

RUN chgrp -R root $(gem env gemdir) /usr/local/lib/ruby \
    && chmod -R g=rwX /opt/app-root $(gem env gemdir) /usr/local/lib/ruby