#!/bin/bash

if [ -n "${BASH_SOURCE[0]}" ]
then
  SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
else
  SCRIPT_DIR="${${(%):-%x}:A:h}"
fi
source ${SCRIPT_DIR}/architecture
echo "running in architecture ${architecture} with docker-compose arguments ${m1_mac_env}"
docker-compose run --rm ${m1_mac_env} rails db:create
docker-compose run --rm ${m1_mac_env} rails db:migrate
docker-compose run --rm ${m1_mac_env} rails db:seed
docker-compose up -d server
echo "visit http://localhost:3000 in your browser"
