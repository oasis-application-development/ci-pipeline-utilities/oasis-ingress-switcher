Rails.application.routes.draw do
  resources :deployments, as: 'oasis_switchable_deployments', only: [:show, :create, :destroy]
  resources :switchers, as: 'oasis_switchers'
  root 'switchers#index'
end
