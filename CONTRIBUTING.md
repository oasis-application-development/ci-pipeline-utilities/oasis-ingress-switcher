# Contributing
## Feature Branch and Merge Request

To make a change in the application, create an issue in the project repo describing the change, then create an merge request and feature branch prefixed with the issue number. Once you have made the changes and are ready to merge, mark the MR as ready and get a review by another team memember on this project.

## Local Development

You must be on the VPN, or on a VDI with access to the dhts network. You will need
to create a file `k8s.env` (see k8s.env.sample), with the token, cluster api url, and namespace where you will be running the server against. This token must be for
a user or service account with the admin role in the namespace.

launch the server
```
bin/docker-compose/start_server.sh
```

Set up aliases for common rails commands to equivalent docker-compose commands.
```
source bin/docker-compose/aliases
rails c
rspec [<path>]
```

Create example deployments. These are designed to simulate 2 instances of the same application, each with the same label `oasis.duke.edu/application-name` value `whoami`, but different label `oasis.duke.edu/application-environment` values. You
can use one or both of these labels in the form to create new unmanaged deployments.
```
kubectl create -f examples/whoami-review-a.yml
kubectl create -f examples/whoami-review-b.yml
``` 

These deployments will not show up in the list of deployments until you create new
oasis-ingress-switcher managed deployments using the form and one of their labels.
For example, to get both of these into the Blockly canvas as deployment options,
create new deployments with "oasis.duke.edu/application-name=whoami".