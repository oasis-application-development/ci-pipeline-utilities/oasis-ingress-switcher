require 'rails_helper'

RSpec.describe "Switchers", type: :request do
  let(:switcher_id) { Faker::Artist.name }
  include_context 'with mocked k8s_watcher'

  describe "GET oasis_switchers_path" do
    let(:switcher) { FactoryBot.build(:switcher) }

    before(:each) do
      mocked_k8s_watcher.routes = [switcher]
    end
    it "returns http success" do
      get oasis_switchers_path
      expect(response).to have_http_status(:success)
    end
  end

  describe 'GET oasis_switcher_path' do
    context 'Oasis::UnknownSwitcherError raised' do
      before do
        expect(Oasis::Switcher).to receive(:find).with(switcher_id) do 
          raise Oasis::UnknownSwitcherError.new("object not found")
        end
      end

      it "returns http 404" do
        get oasis_switcher_path(switcher_id)
        expect(response).to have_http_status(:not_found)
      end
    end

    context 'Oasis::SwitcherError raised' do
      before do
        expect(Oasis::Switcher).to receive(:find).with(switcher_id) do 
          raise Oasis::SwitcherError.new("method not allowed")
        end
      end

      it "returns http unprocessable_entity" do
        get oasis_switcher_path(switcher_id)
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    context 'switcher exists' do
      let(:switcher) { FactoryBot.build(:switcher, name: switcher_id) }

      before do
        mocked_k8s_watcher.routes = [switcher]
      end
      it "returns http success" do
        get oasis_switcher_path(switcher_id)
        expect(response).to have_http_status(:success)
      end
    end
  end

  describe 'PATCH /update' do
    let(:switcher_id) { Faker::Artist.name }
    let(:request_params) { { oasis_switcher: new_attributes } }
    let(:new_attributes) { {} }
    let(:headers) { {} }
    let(:call_request) { patch oasis_switcher_url(switcher_id), params: request_params, headers: headers }

    context 'Oasis::UnknownSwitcherError raised' do
      before do
        expect(Oasis::Switcher).to receive(:find).with(switcher_id) do 
          raise Oasis::UnknownSwitcherError.new("object not found")
        end
      end

      it "returns http 404" do
        call_request
        expect(response).to have_http_status(:not_found)
      end
    end

    context 'Oasis::SwitcherError raised' do
      before do
        expect(Oasis::Switcher).to receive(:find).with(switcher_id) do 
          raise Oasis::SwitcherError.new("method not allowed")
        end
      end

      it "returns http unprocessable_entity" do
        call_request
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    context 'switcher exists' do
      let(:switcher) { FactoryBot.build(:switcher, name: switcher_id) }

      context 'with deployment_id' do
        context 'for existing deployment' do
          let(:deployment) {FactoryBot.build(:deployment) }
          let(:new_attributes) do
            {
              managed_deployment_id: deployment.id
            }
          end

          before do
            mocked_k8s_watcher.routes = [switcher]
            mocked_k8s_watcher.all_deployments = [deployment]
            expect(deployment).to receive(:update).with({labels: { switcher.managing_label => 'true' }}).and_return(true)
          end
          context 'standard request' do
            it "redirects to show" do
              call_request
              expect(response).to redirect_to(oasis_switcher_url(switcher))
            end
          end

          context 'turbo request' do
            include_context 'turbo_frame_request_headers'

            it "removes deployment from unmanaged deployments" do
              call_request
              expect(response).to be_successful
              expect(response.body).to include('turbo-stream action="replace"')
              expect(response.body).to include(switcher.id)
              expect(response.body).to include('turbo-stream action="remove"')
              expect(response.body).to include(deployment.id)
            end
          end
        end

        context 'for non existent deployment' do
          let(:deployment_id) { Faker::Artist.name }
          let(:new_attributes) do
            {
              managed_deployment_id: deployment_id
            }
          end

          before do
            mocked_k8s_watcher.routes = [switcher]
            expect(Oasis::SwitchableDeployment).to receive(:find).with(deployment_id) do
              raise Oasis::UnknownSwitchableDeploymentError.new("object not found")
            end
          end
          it "returns returns http 404" do
            call_request
            expect(response).to have_http_status(:not_found)
          end
        end
        
        context 'nil' do
          let(:deployment) {FactoryBot.build(:deployment) }
          let(:new_attributes) do
            {
              managed_deployment_id: nil
            }
          end

          before do
            mocked_k8s_watcher.deployment_managed_by(switcher_id, deployment)
            switcher.managed_deployment_id = deployment.id
            switcher.managed_deployment = deployment
            mocked_k8s_watcher.routes = [switcher]
            expect(deployment).to receive(:update).with({labels: { switcher.managing_label => nil }}).and_return(true)
          end

          context 'standard request' do
            it "redirects to show" do
              call_request
              expect(response).to redirect_to(oasis_switcher_url(switcher))
            end
          end

          context 'turbo request' do
            include_context 'turbo_frame_request_headers'

            it "adds deployment from unmanaged deployments" do
              call_request
              expect(response).to be_successful
              expect(response.body).to include('turbo-stream action="replace"')
              expect(response.body).to include(switcher.id)
              expect(response.body).to include('turbo-stream action="append"')
              expect(response.body).to include(deployment.id)
            end
          end
        end

        context 'empty string' do
          let(:deployment) {FactoryBot.build(:deployment) }
          let(:new_attributes) do
            {
              managed_deployment_id: ""
            }
          end

          before do
            mocked_k8s_watcher.deployment_managed_by(switcher_id, deployment)
            switcher.managed_deployment = deployment
            switcher.managed_deployment_id = deployment.id
            mocked_k8s_watcher.routes = [switcher]
            expect(deployment).to receive(:update).with({labels: { switcher.managing_label => nil }}).and_return(true)
          end

          context 'standard request' do
            it "redirects to show" do
              call_request
              expect(response).to redirect_to(oasis_switcher_url(switcher))
            end
          end

          context 'turbo request' do
            include_context 'turbo_frame_request_headers'

            it "adds deployment from unmanaged deployments" do
              call_request
              expect(response).to be_successful
              expect(response.body).to include('turbo-stream action="replace"')
              expect(response.body).to include(switcher.id)
              expect(response.body).to include('turbo-stream action="append"')
              expect(response.body).to include(deployment.id)
            end
          end
        end
      end
    end
  end

  describe 'POST /create' do
    let(:request_params) { { oasis_switcher: new_attributes } }
    let(:expected_params) do
      ActionController::Parameters.new(new_attributes).permit(%i[name port]).to_h.symbolize_keys
    end
    let(:headers) { {} }
    let(:call_request) { post oasis_switchers_url, params: request_params, headers: headers }
    let(:name) { Faker::Artist.name }
    let(:port) { '3000' }

    context 'invalid parameters' do
      let(:new_attributes) do
        {
          port: port
        }
      end

      it "returns http unprocessable_entity" do
        call_request
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    context 'valid parameters' do
      let(:new_attributes) do
        {
          name: name,
          port: port
        }
      end
      let(:created_switcher) { FactoryBot.build(:switcher, name: name) }

      context 'not turbo_frame request' do
        before do
          expect(Oasis::Switcher).to receive(:new)
            .with(expected_params)
            .and_return(created_switcher)
          expect(created_switcher).to receive(:save)
            .and_return(true)
        end

        it "redirects to show" do
          call_request
          expect(response).to redirect_to(oasis_switcher_url(name))
        end
      end

      context 'turbo_frame request' do
        include_context 'turbo_frame_request_headers'

        before do
          allow(Oasis::Switcher).to receive(:new)
            .with(expected_params)
            .and_return(created_switcher)
          allow(Oasis::Switcher).to receive(:new)
            .with(no_args).and_call_original
          expect(created_switcher).to receive(:save)
            .and_return(true)
        end
        it 'adds a switcher to the list' do
          call_request
          expect(response).to be_successful
          expect(response.body).to include('turbo-stream action="append"')
          expect(response.body).to include(name)
        end
      end  
    end
  end

  describe 'DELETE /destroy' do
    let(:switcher) { FactoryBot.build(:switcher, name: switcher_id) }
    let(:headers) { {} }
    let(:call_request) { delete oasis_switcher_url(switcher.id), headers: headers }

    before do
      mocked_k8s_watcher.routes = [ switcher ]
    end

    context 'not a turbo request' do
      it "redirects to index" do
        call_request
        expect(response).to redirect_to(oasis_switchers_url)
      end
    end

    context 'turbo request' do
      include_context 'turbo_frame_request_headers'

      it 'deletes an element from the routes' do
        call_request
        expect(response).to be_successful
        expect(response.body).to include('turbo-stream action="remove"')
        expect(response.body).to include(switcher_id)
      end
    end
  end
end
