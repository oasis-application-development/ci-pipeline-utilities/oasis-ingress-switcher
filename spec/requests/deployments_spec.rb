require 'rails_helper'

RSpec.describe "Deployments", type: :request do
  let(:deployment_id) { Faker::Artist.name }
  include_context 'with mocked k8s_watcher'

  describe "POST /create" do
    let(:request_params) { { oasis_switchable_deployment: new_attributes } }
    let(:expected_params) do
      ActionController::Parameters.new(new_attributes).permit(%i[labels]).to_h.symbolize_keys
    end
    let(:call_request) { post oasis_switchable_deployments_url, params: request_params, headers: headers }
    let(:labels) {'label=value' }
    let(:new_attributes) do
      {
        labels: labels
      }
    end
    include_context 'turbo_frame_request_headers'

    context 'Oasis::SwitchableDeploymentError' do
      let(:expected_message) { 'method not allowed' }
      before do
        allow(Oasis::SwitchableDeployment).to receive(:create)
          .with(expected_params) do
            raise Oasis::SwitchableDeploymentError.new expected_message
          end
      end
      it "replaces the error_message" do
        call_request
        expect(response.body).to include('turbo-stream action="replace"')
        expect(response.body).to include(expected_message)
      end
    end

    context 'Oasis::UnknownSwitchableDeploymentError' do
      let(:expected_message) { 'deployments do not exist with these labels' }
      before do
        allow(Oasis::SwitchableDeployment).to receive(:create)
          .with(expected_params) do
            raise Oasis::UnknownSwitchableDeploymentError.new expected_message
          end
      end
      it "replaces the error_message" do
        call_request
        expect(response.body).to include('turbo-stream action="replace"')
        expect(response.body).to include(expected_message)
      end
    end

    context 'no errors' do
      let(:created_deployment) { FactoryBot.build(:deployment) }

      before do
        allow(Oasis::SwitchableDeployment).to receive(:create)
          .with(expected_params)
          .and_return([created_deployment])
      end
      it 'adds a deployment to the list' do
        call_request
        expect(response).to be_successful
        expect(response.body).to include('turbo-stream action="append"')
        expect(response.body).to include(created_deployment.name)
      end
    end
  end

  describe 'DELETE /destroy' do
    let(:deployment) { FactoryBot.build(:deployment, name: deployment_id) }
    let(:headers) { {} }
    let(:call_request) { delete oasis_switchable_deployment_url(deployment.id), headers: headers }
    include_context 'turbo_frame_request_headers'

    before do
      mocked_k8s_watcher.all_deployments = [ deployment ]
    end
    context 'Oasis::SwitchableDeploymentError' do
      let(:expected_message) { 'method not allowed' }
      before do
        allow(deployment).to receive(:destroy) do
          raise Oasis::SwitchableDeploymentError.new expected_message
        end
      end
      it "replaces the error_message" do
        call_request
        expect(response.body).to include('turbo-stream action="replace"')
        expect(response.body).to include(expected_message)
      end
    end

    context 'Oasis::UnknownSwitchableDeploymentError' do
      let(:expected_message) { 'deployments do not exist with these labels' }
      before do
        allow(deployment).to receive(:destroy) do
          raise Oasis::UnknownSwitchableDeploymentError.new expected_message
        end
      end
      it "replaces the error_message" do
        call_request
        expect(response.body).to include('turbo-stream action="replace"')
        expect(response.body).to include(expected_message)
      end
    end

    context 'no errors' do
      before do
        allow(deployment).to receive(:destroy)
      end
      it 'deletes an element from the unmanaged_deployments' do
        call_request
        expect(response).to be_successful
        expect(response.body).to include('turbo-stream action="remove"')
        expect(response.body).to include(deployment_id)
      end
    end
  end
end
