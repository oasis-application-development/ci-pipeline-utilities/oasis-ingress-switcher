# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DeploymentsController, type: :routing do
  describe 'routing' do
    it 'routes to #show' do
      expect(get: oasis_switchable_deployment_path('name')).to route_to('deployments#show', id: 'name')
    end

    it 'routes to #create' do
      expect(post: oasis_switchable_deployments_path).to route_to('deployments#create')
    end

    it 'routes to #destroy' do
      expect(delete: oasis_switchable_deployment_path('name')).to route_to('deployments#destroy', id: 'name')
    end
  end
end
