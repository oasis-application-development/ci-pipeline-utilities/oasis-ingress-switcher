# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SwitchersController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: oasis_switchers_path).to route_to('switchers#index')
      expect(get: '/').to route_to('switchers#index')
    end

    it 'routes to #show' do
      expect(get: oasis_switcher_path('name')).to route_to('switchers#show', id: 'name')
    end

    it 'routes to #create' do
      expect(post: oasis_switchers_path).to route_to('switchers#create')
    end

    it 'routes to #update via PUT' do
      expect(put: oasis_switcher_path('name')).to route_to('switchers#update', id: 'name')
    end

    it 'routes to #update via PATCH' do
      expect(patch: oasis_switcher_path('name')).to route_to('switchers#update', id: 'name')
    end

    it 'routes to #destroy' do
      expect(delete: oasis_switcher_path('name')).to route_to('switchers#destroy', id: 'name')
    end
  end
end
