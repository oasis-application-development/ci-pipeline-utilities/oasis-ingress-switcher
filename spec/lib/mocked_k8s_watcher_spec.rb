require 'rails_helper'

RSpec.describe MockedK8sWatcher do
  subject { MockedK8sWatcher.new }

  it 'mocks all instance methods of K8sWatcher' do
    expect(K8sWatcher.instance_methods - MockedK8sWatcher.instance_methods).to be_empty
  end

  it { is_expected.to respond_to(:routes=).with(1).argument }
  describe '#routes=' do
    let(:routes) do
      [
        instance_double('Oasis::Switcher', id: Faker::Artist.name),
        instance_double('Oasis::Switcher', id: Faker::Artist.name)
      ]
    end

    it 'sets routes returned by list_routes and find_route(id)' do
      expect(subject.list_routes).to be_empty
      routes.each do |r|
        expect(subject.find_route(r.id)).to be_nil
      end
      subject.routes = routes
      expect(subject.list_routes).not_to be_empty
      subject.list_routes.each do |route|
        expect(routes).to include(route)
      end
      routes.each do |r|
        expect(subject.find_route(r.id)).to be
        expect(subject.find_route(r.id).id).to eq(r.id)
      end
    end
  end

  it { is_expected.to respond_to(:deployment_managed_by).with(2).arguments }
  describe '#deployment_managed_by(name, deployment)' do
    let(:names) do 
      {
        Faker::Artist.name => FactoryBot.build(:deployment),
        Faker::Artist.name => FactoryBot.build(:deployment)
      }
    end

    it 'mocks get_deployment_managed_by to return a deployment when passed the given name and list_managed_deployments to returned any deployment set with this method' do
      expect(subject.list_managed_deployments).to be_empty
      names.each do |name, dep|
        expect(subject.get_deployment_managed_by(name)).to be_nil
        subject.deployment_managed_by(name, dep)
        expect(subject.get_deployment_managed_by(name)).to be
        expect(subject.get_deployment_managed_by(name).id).to eq(dep.id)
      end
      expect(subject.list_managed_deployments).not_to be_empty
      subject.list_managed_deployments.each do |md|
        expect(names.values).to include(md)
      end
    end
  end

  it { is_expected.to respond_to(:labeled_deployments=) }
  describe '#labeled_deployments=' do
    let(:selector) { Hash[*Faker::Lorem.words(number: 4)] }
    let(:expected_deployments) { FactoryBot.build_pair(:deployment) }
    let(:labeled_deployments) do
      {
        selector => expected_deployments
      }
    end

    it 'sets response for get_deployments_with_labels(selector)' do
      labeled_deployments.keys.each do |labels|
        expect(subject.get_deployments_with_labels(labels)).to be_empty
      end
      subject.labeled_deployments = labeled_deployments
      expect(subject.get_deployments_with_labels(selector)).not_to be_empty
      expect(subject.get_deployments_with_labels(selector)).to include(*expected_deployments)
    end
  end

  it { is_expected.to respond_to(:all_deployments=) }
  describe '#all_deployments=' do
    let(:deployments) { FactoryBot.build_pair(:deployment) }
    it 'mocks return by find_deployment(id)' do
      deployments.each do |d|
        expect{subject.find_deployment(d.id)}.to raise_error(K8s::Error::NotFound)
      end
      subject.all_deployments=deployments
      deployments.each do |d|
        expect(subject.find_deployment(d.id)).to eq(d)
      end
    end
  end

  it { is_expected.to respond_to(:labeled_pods=) }
  describe '#labeled_pods=' do
    let(:labeled_pods) do
      {
        Faker::Lorem.words(number: 4) => ['pod1','pod2'],
        Faker::Lorem.words(number: 2) => ['pod2','pod3','pod4']      
      }
    end
    it 'mocks return by get_pods_with_labels' do
      labeled_pods.keys.each do |labels|
        expect(subject.get_pods_with_labels(labels)).to be_empty
      end
      subject.labeled_pods=labeled_pods
      labeled_pods.each do |labels, pods|
        expect(subject.get_pods_with_labels(labels)).not_to be_empty
        pods.each do |pod|
          expect(subject.get_pods_with_labels(labels)).to include(pod)
        end
      end
    end
  end

  it { is_expected.to respond_to(:create_route).with_keywords(:name, :port, :labels, :service) }
  it { is_expected.to respond_to(:delete_route).with_keywords(:name) }
  it { is_expected.to respond_to(:create_service).with_keywords(:name, :port, :labels, :selector) }
  it { is_expected.to respond_to(:delete_service).with_keywords(:name) }

  it { is_expected.to respond_to(:reset) }
  describe '#reset' do
    let(:routes) do
      [
        instance_double('Oasis::Switcher', id: Faker::Artist.name),
        instance_double('Oasis::Switcher', id: Faker::Artist.name)
      ]
    end
    let(:names) do 
      {
        Faker::Artist.name => FactoryBot.build(:deployment),
        Faker::Artist.name => FactoryBot.build(:deployment)
      }
    end
    let(:labeled_deployments) do
      {
        Faker::Lorem.words(number: 4) => FactoryBot.build_pair(:deployment),
        Faker::Lorem.words(number: 2) => FactoryBot.build_pair(:deployment),
      }
    end
    let(:labeled_pods) do
      {
        Faker::Lorem.words(number: 4) => ['pod1','pod2'],
        Faker::Lorem.words(number: 2) => ['pod2','pod3','pod4']      
      }
    end
    it 'sets everything to a pristine state' do
      subject.routes = routes
      names.map {|n,d| subject.deployment_managed_by(n,d) }
      subject.labeled_deployments = labeled_deployments
      subject.all_deployments = labeled_deployments.values.first
      subject.labeled_pods = labeled_pods
      subject.reset
      expect(subject.list_routes).to be_empty
      expect(subject.list_managed_deployments).to be_empty
      labeled_deployments.keys.each do |l|
        expect(subject.get_deployments_with_labels(l)).to be_empty
      end
      labeled_deployments.values.first.each do |d|
        expect{ subject.find_deployment(d.id) }.to raise_error(K8s::Error::NotFound)
      end
      labeled_pods.keys.each do |labels|
        expect(subject.get_deployments_with_labels(labels)).to be_empty
      end
    end
  end
end
