require 'rails_helper'

# Caution, This test mocks what we do not own
RSpec.describe K8sWatcher do
  #https://stackoverflow.com/a/23901644
  subject { Class.new(K8sWatcher).instance }
  let(:namespace) { Faker::Hobby.activity }
  let(:expected_namespace_path) { '/var/run/secrets/kubernetes.io/serviceaccount/namespace' }
  let(:apps_group) { 'apps/v1' }
  let(:core_group) { 'v1' }
  let(:routes_group) { 'route.openshift.io/v1' }

  before(:each) do
    allow(File).to receive(:read).with(expected_namespace_path).and_return(namespace)
  end

  describe 'api' do
    it { expect(described_class).to respond_to(:instance) }

    describe 'in_cluster_config' do
      let(:expected_namespace_path) { '/var/run/secrets/kubernetes.io/serviceaccount/namespace' }
      let(:in_cluster_mocked_k8s_client) { instance_double('K8s::Client') }
      before(:each) do    
        allow(K8s::Client).to receive(:in_cluster_config).and_return(in_cluster_mocked_k8s_client)
        allow(in_cluster_mocked_k8s_client).to receive(:apis).with(prefetch_resources: true)
      end
    
      it { is_expected.to respond_to(:client) }
      it {expect(subject.client).to eq(in_cluster_mocked_k8s_client) }

      it { is_expected.to respond_to(:namespace) }
      it { expect(subject.namespace).to eq(namespace) }

      it { is_expected.to respond_to(:list_routes) }
      it { is_expected.to respond_to(:create_route).with_keywords(:name, :port, :labels, :service) }
      it { is_expected.to respond_to(:delete_route).with_keywords(:name) }
      it { is_expected.to respond_to(:create_service).with_keywords(:name, :port, :labels, :selector) }
      it { is_expected.to respond_to(:delete_service).with_keywords(:name) }
      it { is_expected.to respond_to(:list_managed_deployments) }
      it { is_expected.to respond_to(:get_deployment_managed_by).with(1).argument }
      it { is_expected.not_to respond_to(:get_deployment_managed_by).with(0).arguments }
      it { is_expected.to respond_to(:get_deployments_with_labels).with(1).argument }
      it { is_expected.not_to respond_to(:get_deployments_with_labels).with(0).arguments }
      it { is_expected.to respond_to(:find_route).with(1).argument }
      it { is_expected.not_to respond_to(:find_route).with(0).arguments }
      it { is_expected.to respond_to(:find_deployment).with(1).argument }
      it { is_expected.not_to respond_to(:find_deployment).with(0).arguments }
      it { is_expected.to respond_to(:get_pods_with_labels).with(1).argument }
      it { is_expected.not_to respond_to(:get_pods_with_labels).with(0).arguments }
      it { is_expected.to respond_to(:patch_pod_labels).with(2).arguments }
      it { is_expected.not_to respond_to(:patch_pod_labels).with(1).argument }
      it { is_expected.not_to respond_to(:patch_pod_labels).with(0).arguments }
      it { is_expected.to respond_to(:patch_deployment_labels).with(2).arguments }
      it { is_expected.not_to respond_to(:patch_deployment_labels).with(1).argument }
      it { is_expected.not_to respond_to(:patch_deployment_labels).with(0).arguments }
    end

    describe 'external_config' do
      include_context 'with env_override'
      let(:host) { Faker::Internet.url }
      let(:token) { SecureRandom.hex }
      let(:expected_config) do
        K8s::Config.new(
          clusters: [{
              name: "#{namespace}-cluster",
              cluster: {server: host}
          }],
          users: [{
              name: "#{namespace}-user",
              user: {token: token}
          }],
          contexts: [{
              name: "#{namespace}-context",
              context: {
                  cluster: "#{namespace}-cluster",
                  user: "#{namespace}-user"
              }
          }],
          current_context: "#{namespace}-context"
        )
      end
      let(:env_override) do
        {
          K8S_TOKEN: token,
          K8S_API_URL: host,
          K8S_NAMESPACE: namespace
        }
      end
      let(:external_mocked_k8s_client) { instance_double('K8s::Client') }

      before(:each) do
        allow(K8s::Client).to receive(:config).with(expected_config, persistent: true).and_return(external_mocked_k8s_client)
        allow(external_mocked_k8s_client).to receive(:apis).with(prefetch_resources: true)    
      end
        
      it { is_expected.to respond_to(:client) }
      it {expect(subject.client).to eq(external_mocked_k8s_client) }
    end
  end

  describe 'instance' do
    describe '#list_routes' do
      let(:expected_routes_selector) do
        {
          'oasis.duke.edu/ingress-switcher' => 'true'
        }
      end
      let(:expected_routes_list) { FactoryBot.build_pair(:switcher, :object) }
      let(:input_routes_resources) do
        expected_routes_list.map(&:object)
      end
      let(:call) { subject.list_routes }

      before(:each) do
        mocked_k8s_client = instance_double('K8s::Client')
        mocked_route_api = instance_double('K8s::APIClient')
        mocked_routes_resource = instance_double('K8s::ResourceClient')

        allow(K8s::Client).to receive(:in_cluster_config).and_return(mocked_k8s_client)
        allow(mocked_k8s_client).to receive(:apis).with(prefetch_resources: true)

        allow(K8sWatcher).to receive(:instance).and_return(subject)
        allow(subject).to receive(:get_deployment_managed_by).and_return(nil)

        allow(mocked_k8s_client).to receive(:api).with(routes_group).and_return(mocked_route_api)
        allow(mocked_route_api).to receive(:resource).with('routes', namespace: namespace).and_return(mocked_routes_resource)    
        allow(mocked_routes_resource).to receive(:list).with(labelSelector: expected_routes_selector).and_return(input_routes_resources)
      end
    
      it 'returns an Array of Oasis::Switcher objects' do
        expect(call).to be_an(Array)
        expect(call).to all(be_an(Oasis::Switcher))
        expect(call.first.name).to eq(expected_routes_list.first.name)
        expect(call.last.name).to eq(expected_routes_list.last.name)
      end
    end

    describe '#create_route' do
      let(:expected_name) { Faker::Artist.name }
      let(:expected_service_name) { Faker::Artist.name }
      let(:expected_port_name) { Faker::Hobby.activity }
      let(:expected_labels) do
        {
          :"oasis.duke.edu/ingress-switcher" => 'true',
          :"oasis.duke.edu/ingress-switcher-name" => expected_name
        }
      end
      let(:expected_spec) do
        {
          apiVersion: 'route.openshift.io/v1',
          kind: 'Route',
          metadata: {
            namespace: namespace,
            name: expected_name,
            labels: expected_labels
          },
          spec: {
            port: {
              targetPort: expected_port_name
            },
            tls: {
              insecureEdgeTerminationPolicy: 'Redirect',
              termination: 'edge'
            },
            to: {
              kind: 'Service',
              name: expected_service_name,
              weight: 100
            },
            wildcardPolicy: 'None'
          },
          status: {
            ingress: [
              {wildcardPolicy: 'None'}
            ]
          }
        }
      end
      let(:expected_resource) { K8s::Resource.new(expected_spec) }
      let(:call) do
        subject.create_route(
          name: expected_name, 
          port: expected_port_name, 
          labels: expected_labels,
          service: expected_service_name
        )
      end

      before(:each) do
        mocked_k8s_client = instance_double('K8s::Client')
        mocked_route_api = instance_double('K8s::APIClient')
        mocked_routes_resource = instance_double('K8s::ResourceClient')

        allow(K8s::Client).to receive(:in_cluster_config).and_return(mocked_k8s_client)
        allow(mocked_k8s_client).to receive(:apis).with(prefetch_resources: true)

        allow(K8s::Resource).to receive(:new)
          .with(expected_spec)
          .and_return(expected_resource)
        allow(mocked_k8s_client).to receive(:api).with(routes_group).and_return(mocked_route_api)
        allow(mocked_route_api).to receive(:resource).with('routes', namespace: namespace).and_return(mocked_routes_resource)
        allow(mocked_routes_resource).to receive(:create_resource).with(expected_resource).and_return(expected_resource)
      end

      it 'returns the newly created resource' do
        expect {
          expect(call).to eq(expected_resource)
        }.not_to raise_error
      end
    end

    describe '#delete_route' do
      let(:name) { Faker::Artist.name }
      let(:call) { subject.delete_route(name: name) }

      context 'K8s::Error::NotFound raised' do
        let(:expected_error) { K8s::Error::NotFound.new('method','path','code','reason') }

        before(:each) do
          mocked_k8s_client = instance_double('K8s::Client')
          mocked_route_api = instance_double('K8s::APIClient')
          mocked_routes_resource = instance_double('K8s::ResourceClient')
  
          allow(K8s::Client).to receive(:in_cluster_config).and_return(mocked_k8s_client)
          allow(mocked_k8s_client).to receive(:apis).with(prefetch_resources: true)
  
          allow(mocked_k8s_client).to receive(:api).with(routes_group).and_return(mocked_route_api)
          allow(mocked_route_api).to receive(:resource).with('routes', namespace: namespace).and_return(mocked_routes_resource)
          allow(mocked_routes_resource).to receive(:delete).with(name) do
            raise expected_error
          end
        end

        it 'returns true without raising an exception' do
          expect {
            expect(call).to be_truthy
          }.not_to raise_error
        end
      end

      context 'other K8s::Error raised' do
        let(:expected_error) { K8s::Error::Forbidden.new('method','path','code','reason') }

        before(:each) do
          mocked_k8s_client = instance_double('K8s::Client')
          mocked_route_api = instance_double('K8s::APIClient')
          mocked_routes_resource = instance_double('K8s::ResourceClient')
  
          allow(K8s::Client).to receive(:in_cluster_config).and_return(mocked_k8s_client)
          allow(mocked_k8s_client).to receive(:apis).with(prefetch_resources: true)
  
          allow(mocked_k8s_client).to receive(:api).with(routes_group).and_return(mocked_route_api)
          allow(mocked_route_api).to receive(:resource).with('routes', namespace: namespace).and_return(mocked_routes_resource)
          allow(mocked_routes_resource).to receive(:delete).with(name) do
            raise expected_error
          end
        end

        it 'returns true without raising an exception' do
          expect {
            call
          }.to raise_error(expected_error)
        end
      end

      context 'success' do
        let(:expected_error) { K8s::Error::NotFound.new('method','path','code','reason') }

        before(:each) do
          mocked_k8s_client = instance_double('K8s::Client')
          mocked_route_api = instance_double('K8s::APIClient')
          mocked_routes_resource = instance_double('K8s::ResourceClient')
  
          allow(K8s::Client).to receive(:in_cluster_config).and_return(mocked_k8s_client)
          allow(mocked_k8s_client).to receive(:apis).with(prefetch_resources: true)
  
          allow(mocked_k8s_client).to receive(:api).with(routes_group).and_return(mocked_route_api)
          allow(mocked_route_api).to receive(:resource).with('routes', namespace: namespace).and_return(mocked_routes_resource)
          mocked_deleted_object = instance_double('K8s::Resource')
          allow(mocked_routes_resource).to receive(:delete).with(name).and_return(mocked_deleted_object)
        end

        it 'returns true without raising an exception' do
          expect {
            expect(call).to be_truthy
          }.not_to raise_error
        end
      end
    end

    describe '#create_service' do
      let(:expected_name) { Faker::Artist.name }
      let(:expected_port) { '8080' }
      let(:expected_labels) do
        {
          :"oasis.duke.edu/ingress-switcher" => 'true',
          :"oasis.duke.edu/ingress-switcher-name" => expected_name
        }
      end
      let(:expected_selector) do
        {
          :"oasis.duke.edu/ingress-switcher-managed" => 'true',
          :"oasis.duke.edu/test-switcher-one-managed" => 'true'              
        }
      end
      let(:expected_spec) do
        {
          apiVersion: 'v1',
          kind: 'Service',
          metadata: {
            namespace: namespace,
            name: expected_name,
            labels: expected_labels
          },
          spec: {
            type: 'ClusterIP',
            ports: [
              {
                port: expected_port.to_i,
                targetPort: expected_port.to_i,
                name: 'http'
              },
            ],
            selector: expected_selector
          }
        }
      end
      let(:expected_resource) { K8s::Resource.new(expected_spec) }

      let(:call) do
        subject.create_service(
          name: expected_name, 
          port: expected_port,
          labels: expected_labels, 
          selector: expected_selector
        ) 
      end

      before(:each) do
        mocked_k8s_client = instance_double('K8s::Client')
        mocked_core_api = instance_double('K8s::APIClient')
        mocked_core_resource = instance_double('K8s::ResourceClient')  
    
        allow(K8s::Client).to receive(:in_cluster_config).and_return(mocked_k8s_client)
        allow(mocked_k8s_client).to receive(:apis).with(prefetch_resources: true)

        allow(K8sWatcher).to receive(:instance).and_return(subject)

        expect(K8s::Resource).to receive(:new)
          .with(expected_spec)
          .and_return(expected_resource)
        allow(mocked_k8s_client).to receive(:api).with(core_group).and_return(mocked_core_api)
        allow(mocked_core_api).to receive(:resource).with('services', namespace: namespace).and_return(mocked_core_resource)
        allow(mocked_core_resource).to receive(:create_resource).with(expected_resource).and_return(expected_resource)
      end

      it 'returns true' do
        expect {
          expect(call).to be_truthy
        }.not_to raise_error
      end
    end

    describe '#delete_service' do
      let(:name) { Faker::Artist.name }
      let(:call) { subject.delete_service(name: name) }

      context 'K8s::Error::NotFound raised' do
        let(:expected_error) { K8s::Error::NotFound.new('method','path','code','reason') }

        before(:each) do
          mocked_k8s_client = instance_double('K8s::Client')
          mocked_core_api = instance_double('K8s::APIClient')
          mocked_services_resource = instance_double('K8s::ResourceClient')

          allow(K8s::Client).to receive(:in_cluster_config).and_return(mocked_k8s_client)
          allow(mocked_k8s_client).to receive(:apis).with(prefetch_resources: true)

          allow(mocked_k8s_client).to receive(:api).with(core_group).and_return(mocked_core_api)
          allow(mocked_core_api).to receive(:resource).with('services', namespace: namespace).and_return(mocked_services_resource)
          allow(mocked_services_resource).to receive(:delete).with(name) do
            raise expected_error
          end
        end

        it 'returns true without raising an exception' do
          expect {
            expect(call).to be_truthy
          }.not_to raise_error
        end
      end

      context 'other K8s::Error raised' do
        let(:expected_error) { K8s::Error::Forbidden.new('method','path','code','reason') }

        before(:each) do
          mocked_k8s_client = instance_double('K8s::Client')
          mocked_core_api = instance_double('K8s::APIClient')
          mocked_services_resource = instance_double('K8s::ResourceClient')

          allow(K8s::Client).to receive(:in_cluster_config).and_return(mocked_k8s_client)
          allow(mocked_k8s_client).to receive(:apis).with(prefetch_resources: true)

          allow(mocked_k8s_client).to receive(:api).with(core_group).and_return(mocked_core_api)
          allow(mocked_core_api).to receive(:resource).with('services', namespace: namespace).and_return(mocked_services_resource)
          allow(mocked_services_resource).to receive(:delete).with(name) do
            raise expected_error
          end
        end
        it 'returns true without raising an exception' do
          expect {
            call
          }.to raise_error(expected_error)
        end
      end

      context 'success' do
        let(:expected_error) { K8s::Error::NotFound.new('method','path','code','reason') }

        
        before(:each) do
          mocked_k8s_client = instance_double('K8s::Client')
          mocked_core_api = instance_double('K8s::APIClient')
          mocked_services_resource = instance_double('K8s::ResourceClient')

          allow(K8s::Client).to receive(:in_cluster_config).and_return(mocked_k8s_client)
          allow(mocked_k8s_client).to receive(:apis).with(prefetch_resources: true)

          allow(mocked_k8s_client).to receive(:api).with(core_group).and_return(mocked_core_api)
          allow(mocked_core_api).to receive(:resource).with('services', namespace: namespace).and_return(mocked_services_resource)
          mocked_deleted_object = instance_double('K8s::Resource')
          allow(mocked_services_resource).to receive(:delete).with(name).and_return(mocked_deleted_object)
        end

        it 'returns true without raising an exception' do
          expect {
            expect(call).to be_truthy
          }.not_to raise_error
        end
      end
    end

    describe '#find_route' do
      let(:id) { Faker::Artist.name }
      let(:call) { subject.find_route(id) }

      before(:each) do
        mocked_k8s_client = instance_double('K8s::Client')
        mocked_routes_api = instance_double('K8s::APIClient')
        mocked_routes_resource = instance_double('K8s::ResourceClient')  
    
        allow(K8s::Client).to receive(:in_cluster_config).and_return(mocked_k8s_client)
        allow(mocked_k8s_client).to receive(:apis).with(prefetch_resources: true)

        allow(K8sWatcher).to receive(:instance).and_return(subject)
        allow(subject).to receive(:get_deployment_managed_by).and_return(nil)

        allow(mocked_k8s_client).to receive(:api).with(routes_group).and_return(mocked_routes_api)
        allow(mocked_routes_api).to receive(:resource).with('routes', namespace: namespace).and_return(mocked_routes_resource)
        if expected_error
          allow(mocked_routes_resource).to receive(:get).with(id) do
            raise expected_error
          end
        else
          allow(mocked_routes_resource).to receive(:get).with(id).and_return(expected_route_object)
        end
      end
    
      context 'when route does not exist' do
        let(:expected_route) { nil }
        let(:expected_error) { K8s::Error::NotFound.new('method','path','code','reason') }
        let(:expected_route_object) { nil }

        it 'raises K8s::Error::NotFound' do
          expect{call}.to raise_error(expected_error)
        end
      end

      context 'when route exists' do
        let(:expected_route) { FactoryBot.build(:switcher, :object) }
        let(:expected_error) { nil }
        let(:expected_route_object) { expected_route.object }

        it 'returns an Oasis::SwitchableDeployment object' do
          expect(call).to be_an(Oasis::Switcher)
          expect(call.id).to eq(expected_route.id)
        end
      end
    end

    describe '#list_managed_deployments' do
      let(:expected_all_selector) do
        {
          'oasis.duke.edu/ingress-switcher-managed' => 'true'
        }
      end
      let(:expected_all_list) { FactoryBot.build_pair(:deployment) }
      let(:input_all_resources) do
        expected_all_list.map(&:object)
      end
      let(:call) { subject.list_managed_deployments }

      before(:each) do    
        mocked_k8s_client = instance_double('K8s::Client')
        mocked_apps_api = instance_double('K8s::APIClient')
        mocked_deployments_resource = instance_double('K8s::ResourceClient')  
    
        allow(K8s::Client).to receive(:in_cluster_config).and_return(mocked_k8s_client)
        allow(mocked_k8s_client).to receive(:apis).with(prefetch_resources: true)

        allow(mocked_k8s_client).to receive(:api).with(apps_group).and_return(mocked_apps_api)
        allow(mocked_apps_api).to receive(:resource).with('deployments', namespace: namespace).and_return(mocked_deployments_resource)
        allow(mocked_deployments_resource).to receive(:list).with(labelSelector: expected_all_selector).and_return(input_all_resources)
      end
    
      it 'returns a list of Oasis::SwitchableDeployment objects' do
        expect(call).to be_an(Array)
        expect(call).to all(be_an(Oasis::SwitchableDeployment))
        expect(call.first.name).to eq(expected_all_list.first.name)
        expect(call.last.name).to eq(expected_all_list.last.name)
      end
    end

    describe '#get_deployments_with_labels' do
      let(:test_switch) { Faker::Artist.name }
      let(:expected_switcher_label) { "oasis.duke.edu/#{test_switch}-managed" }
      let(:expected_switcher_selector) do
        {
          expected_switcher_label => 'true'
        }
      end
      let(:call) { subject.get_deployments_with_labels(expected_switcher_selector) }

      before(:each) do
        mocked_k8s_client = instance_double('K8s::Client')
        mocked_apps_api = instance_double('K8s::APIClient')
        mocked_deployments_resource = instance_double('K8s::ResourceClient')  
    
        allow(K8s::Client).to receive(:in_cluster_config).and_return(mocked_k8s_client)
        allow(mocked_k8s_client).to receive(:apis).with(prefetch_resources: true)

        allow(mocked_k8s_client).to receive(:api).with(apps_group).and_return(mocked_apps_api)
        allow(mocked_apps_api).to receive(:resource).with('deployments', namespace: namespace).and_return(mocked_deployments_resource)
        allow(mocked_deployments_resource).to receive(:list).with(labelSelector: expected_switcher_selector).and_return(input_switcher_resources)
      end
    
      context 'when no deployment has the label' do
        let(:input_switcher_resources) { [] }

        it 'returns an empty Array' do
          expect(call).to be_an(Array)
          expect(call).to be_empty
        end
      end

      context 'when a deployment has the label' do
        let(:expected_deployment) { FactoryBot.build(:deployment, labels: expected_switcher_selector) }
        let(:input_switcher_resources) do
          [expected_deployment.object]
        end

        it 'returns an Array of Oasis::SwitchableDeployment objects' do
          expect(call).to be_an(Array)
          expect(call).not_to be_empty
          expect(call.first).to be_an(Oasis::SwitchableDeployment)
          expect(call.first.id).to eq(expected_deployment.id)
          expect(call.first.labels[expected_switcher_label]).to eq('true')
        end
      end
    end

    describe '#get_deployment_managed_by' do
      let(:test_switch) { Faker::Artist.name }
      let(:expected_switcher_label) { "oasis.duke.edu/#{test_switch}-managed" }
      let(:expected_switcher_selector) do
        {
          expected_switcher_label => 'true'
        }
      end
      let(:call) { subject.get_deployment_managed_by(test_switch) }

      before(:each) do
        mocked_k8s_client = instance_double('K8s::Client')    
        allow(K8s::Client).to receive(:in_cluster_config).and_return(mocked_k8s_client)
        allow(mocked_k8s_client).to receive(:apis).with(prefetch_resources: true)

        is_expected.to receive(:get_deployments_with_labels)
          .with(expected_switcher_selector)
          .and_return(input_switcher_resources)
      end
    
      context 'when no deployment is managed by test_switch' do
        let(:input_switcher_resources) { [] }

        it 'returns nil' do
          expect(call).to be_nil
        end
      end

      context 'when one deployment is managed by test_switch' do
        let(:expected_deployment) { FactoryBot.build(:deployment, labels: expected_switcher_selector) }
        let(:input_switcher_resources) do
          [expected_deployment]
        end

        it 'returns an Oasis::SwitchableDeployment object' do
          expect(call).to be_an(Oasis::SwitchableDeployment)
          expect(call.id).to eq(expected_deployment.id)
          expect(call.labels[expected_switcher_label]).to eq('true')
        end
      end
    end

    describe '#find_deployment' do
      let(:id) { Faker::Artist.name }
      let(:call) { subject.find_deployment(id) }

      before(:each) do
        mocked_k8s_client = instance_double('K8s::Client')
        mocked_apps_api = instance_double('K8s::APIClient')
        mocked_deployments_resource = instance_double('K8s::ResourceClient')  
    
        allow(K8s::Client).to receive(:in_cluster_config).and_return(mocked_k8s_client)
        allow(mocked_k8s_client).to receive(:apis).with(prefetch_resources: true)

        allow(mocked_k8s_client).to receive(:api).with(apps_group).and_return(mocked_apps_api)
        allow(mocked_apps_api).to receive(:resource).with('deployments', namespace: namespace).and_return(mocked_deployments_resource)

        if expected_error
          allow(mocked_deployments_resource).to receive(:get).with(id) do
            raise expected_error
          end
        else
          allow(mocked_deployments_resource).to receive(:get).with(id).and_return(expected_deployment_object)
        end
      end
    
      context 'when deployment does not exist' do
        let(:expected_deployment) { nil }
        let(:expected_error) { K8s::Error::NotFound.new('method','path','code','reason') }
        let(:expected_deployment_object) { nil }

        it 'returns nil' do
          expect{call}.to raise_error(expected_error)
        end
      end

      context 'when deployment exists' do
        let(:expected_deployment) { FactoryBot.build(:deployment) }
        let(:expected_error) { nil }
        let(:expected_deployment_object) { expected_deployment.object }

        it 'returns an Oasis::SwitchableDeployment object' do
          expect(call).to be_an(Oasis::SwitchableDeployment)
          expect(call.id).to eq(expected_deployment.id)
        end
      end
    end

    describe '#get_pods_with_labels' do
      let(:expected_labels) { Hash[*Faker::Lorem.words(number: 4)] }
      let(:expected_pod_name) { "#{Faker::Artist.name}-#{Faker::Crypto.md5}" }
      let(:call) { subject.get_pods_with_labels(expected_labels) }

      before(:each) do
        mocked_k8s_client = instance_double('K8s::Client')
        mocked_core_api = instance_double('K8s::APIClient')
        mocked_pods_resource = instance_double('K8s::ResourceClient')  
        expected_pod_object = double('K8s::Resource')
        expected_pod_metadata = double('K8s::Resource')
        expected_pod_objects = [expected_pod_object]

        allow(expected_pod_object).to receive(:metadata).and_return(expected_pod_metadata)
        allow(expected_pod_metadata).to receive(:name).and_return(expected_pod_name)
        allow(expected_pod_metadata).to receive(:labels).and_return(expected_labels)

        allow(K8s::Client).to receive(:in_cluster_config).and_return(mocked_k8s_client)
        allow(mocked_k8s_client).to receive(:apis).with(prefetch_resources: true)

        allow(mocked_k8s_client).to receive(:api).with(core_group).and_return(mocked_core_api)
        allow(mocked_core_api).to receive(:resource).with('pods', namespace: namespace).and_return(mocked_pods_resource)
        allow(mocked_pods_resource).to receive(:list).with(labelSelector: expected_labels).and_return(expected_pod_objects)
      end

      it 'returns an Array of pod K8s::Resource objects' do
        expect(call).to be_an(Array)
        expect(call.length).to eq(1)
        expect(call.first.metadata.name).to eq(expected_pod_name)
        expect(call.first.metadata.labels).to eq(expected_labels)
      end
    end

    describe '#patch_pod_labels' do
      let(:expected_labels) { Hash[*Faker::Lorem.words(number: 4)] }
      let(:expected_merge_patch) do 
        {
          metadata: {
            labels: expected_labels
          }
        }
      end
      let(:expected_pod_name) { "#{Faker::Artist.name}-#{Faker::Crypto.md5}" }
      let(:call) { subject.patch_pod_labels(expected_pod_name, expected_labels) }

      before(:each) do
        mocked_k8s_client = instance_double('K8s::Client')
        mocked_core_api = instance_double('K8s::APIClient')
        mocked_pods_resource = instance_double('K8s::ResourceClient')  
        expected_pod_object = double('K8s::Resource')
        expected_pod_metadata = double('K8s::Resource')

        allow(expected_pod_object).to receive(:metadata).and_return(expected_pod_metadata)
        allow(expected_pod_metadata).to receive(:name).and_return(expected_pod_name)
        allow(expected_pod_metadata).to receive(:labels).and_return(expected_labels)

        allow(K8s::Client).to receive(:in_cluster_config).and_return(mocked_k8s_client)
        allow(mocked_k8s_client).to receive(:apis).with(prefetch_resources: true)

        allow(mocked_k8s_client).to receive(:api).with(core_group).and_return(mocked_core_api)
        allow(mocked_core_api).to receive(:resource).with('pods', namespace: namespace).and_return(mocked_pods_resource)
        allow(mocked_pods_resource).to receive(:merge_patch).with(expected_pod_name, expected_merge_patch).and_return(expected_pod_object)
      end

      it 'returns an Array of pod K8s::Resource objects' do
        expect(call.metadata.name).to eq(expected_pod_name)
        expect(call.metadata.labels).to eq(expected_labels)
      end
    end

    describe '#patch_deployment_labels' do
      let(:expected_labels) { Hash[*Faker::Lorem.words(number: 4)] }
      let(:expected_merge_patch) do 
        {
          metadata: {
            labels: expected_labels
          }
        }
      end
      let(:expected_deployment_name) { "#{Faker::Artist.name}" }
      let(:call) { subject.patch_deployment_labels(expected_deployment_name, expected_labels) }

      before(:each) do
        mocked_k8s_client = instance_double('K8s::Client')
        mocked_apps_api = instance_double('K8s::APIClient')
        mocked_deployments_resource = instance_double('K8s::ResourceClient')  
        expected_deployment_object = double('K8s::Resource')
        expected_deployment_metadata = double('K8s::Resource')

        allow(expected_deployment_object).to receive(:metadata).and_return(expected_deployment_metadata)
        allow(expected_deployment_metadata).to receive(:name).and_return(expected_deployment_name)
        allow(expected_deployment_metadata).to receive(:labels).and_return(expected_labels)

        allow(K8s::Client).to receive(:in_cluster_config).and_return(mocked_k8s_client)
        allow(mocked_k8s_client).to receive(:apis).with(prefetch_resources: true)

        allow(mocked_k8s_client).to receive(:api).with(apps_group).and_return(mocked_apps_api)
        allow(mocked_apps_api).to receive(:resource).with('deployments', namespace: namespace).and_return(mocked_deployments_resource)
        allow(mocked_deployments_resource).to receive(:merge_patch).with(expected_deployment_name, expected_merge_patch).and_return(expected_deployment_object)
      end

      it 'returns an Array of pod K8s::Resource objects' do
        expect(call.metadata.name).to eq(expected_deployment_name)
        expect(call.metadata.labels).to eq(expected_labels)
      end
    end
  end
end
