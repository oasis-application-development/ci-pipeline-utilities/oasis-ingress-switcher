require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the K8sWatcherHelper. For example:
#
# describe K8sWatcherHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe K8sWatcherHelper, type: :helper do
  include_context 'with mocked k8s_watcher'

  it { expect(helper).to respond_to(:watcher) }
  describe 'helper.watcher' do
    it { expect(helper.watcher).to eq(mocked_k8s_watcher) }
    it { expect(helper.watcher).to eq(helper.watcher) }
  end
end
