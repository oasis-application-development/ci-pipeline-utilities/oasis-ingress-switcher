# frozen_string_literal: true

shared_context 'with env_override' do
  let(:env_override) { {} }
  around(:example) do |example|
    original_env = ENV.to_hash
    begin
      env_override.each do |env_key, env_val|
        ENV[env_key.to_s] = env_val.to_s
      end
      example.run
    ensure
      ENV.replace original_env
    end
  end
end
