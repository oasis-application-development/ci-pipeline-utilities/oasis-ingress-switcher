shared_context 'with mocked k8s_watcher' do
  let(:mocked_k8s_watcher) { MockedK8sWatcher.new }
  before(:each) do
    allow(K8sWatcher).to receive(:instance).and_return(mocked_k8s_watcher)
  end
  after(:each) do
    mocked_k8s_watcher.reset
  end
end