# frozen_string_literal: true

shared_context 'turbo_frame_request_headers' do
  let(:headers) do
    {
      'Accept' => 'text/vnd.turbo-stream.html'
    }
  end
end

# there are no rspec matchers for turbo-rails yet
# but we need to test that the turbo_stream_from subscribe
# queue matches what the model Broadcastable methods will publish
# so must use a Turbo method to do this, which is a little bit
# implementation specific
shared_examples 'a turbo_stream subscriber' do
  let(:expected_signed_streamable) do
    Turbo::StreamsChannel.signed_stream_name(turbo_stream_streamable)
  end
  let(:expected_stream_index) { 1 }
  it 'renders turbo_stream and turbo_frame subscribed to the turbo_stream_streamable' do
    assert_select "turbo-cable-stream-source[#{expected_stream_index}]" do |turbostream|
      expect(turbostream.attr('signed-stream-name').to_s).to eq(expected_signed_streamable)
    end
  end
end

shared_examples 'a turbo_stream target' do
  it 'renders a turbo_frame with the turbo_stream_target' do
    assert_select 'turbo-frame[id=?]', turbo_stream_target
  end
end
