require 'rails_helper'

def has_managed_deployment
  before do
    subject.managed_deployment_id = managed_deployment.id
    subject.managed_deployment = managed_deployment
  end
end

def raises_unset_managed_deployment_error
  let(:unset_error) { Oasis::SwitchableDeploymentError.new 'error' }

  before(:each) do
    is_expected.to receive(:unset_managed_deployment) do
      raise unset_error
    end
    is_expected.not_to receive(:set_managed_deployment)
  end
end

def does_not_unset_or_set
  before(:each) do
    is_expected.not_to receive(:unset_managed_deployment)
    is_expected.not_to receive(:set_managed_deployment)
  end
end

RSpec.describe Oasis::Switcher do
  let(:switcher) { FactoryBot.build(:switcher, :object) }
	let(:name) { switcher.name }
	let(:host) { switcher.host }
  let(:port) { switcher.port }
	let(:labels) { switcher.labels }
	let(:mocked_route) { switcher.object }
  include_context 'with mocked k8s_watcher'

  describe 'api' do
    subject do
      Oasis::Switcher.new(name: name, port: port)
    end

    it { is_expected.to be_an ActiveModel::Model }
    it { is_expected.to respond_to(:persisted?) }
    it { is_expected.to respond_to(:valid?) }
    it { is_expected.to respond_to(:id) }
    it { is_expected.to respond_to(:to_param) }
    it { is_expected.to respond_to(:port) }
    it { is_expected.to respond_to(:update).with(1).argument }
    it { is_expected.to respond_to(:save) }
    it { is_expected.to respond_to(:destroy) }
    it { expect(described_class).to respond_to(:count) }
    it { expect(described_class).to respond_to(:create).with(1).argument }
    it { expect(described_class).to respond_to(:all) }
    it { expect(described_class).to respond_to(:find).with(1).argument }
    it { expect(described_class).not_to respond_to(:find).with(0).arguments }

    it { is_expected.to be_an ActiveModel::Serializers::JSON }
    it { expect(subject.attributes).to eq({'name' => nil, 'host' => nil, 'port' => nil, 'managed_deployment_id' => nil, 'managed_deployment' => nil}) }

    it { is_expected.to respond_to(:managing_label) }
    it { expect(subject.managing_label).to eq("oasis.duke.edu/#{name}-managed") }
    it { is_expected.to respond_to(:host) }
    it { is_expected.to respond_to(:labels) }
    it { is_expected.to respond_to(:object) }
    it { is_expected.to respond_to(:error) }
    it { expect(described_class).to respond_to(:managed_deployment_label) }
    it { expect(described_class.managed_deployment_label).to eq('oasis.duke.edu/ingress-switcher-managed') }
  end

  describe '#new' do
    context 'without arguments' do
      subject { Oasis::Switcher.new }
  
      it { expect(subject.persisted?).to be_falsey }
      it { expect(subject.valid?).to be_falsey }
      it { expect(subject.id).to be_nil }
      it { expect(subject.to_param).to be_nil }
      it { expect(subject.name).to be_nil }
      it { expect(subject.host).to be_nil }
      it { expect(subject.object).to be_nil }
      it { expect(subject.port).to be_nil }
      it { expect(subject.name).to be_nil }
      it { expect(subject.managed_deployment).to be_nil }
    end
  
    context 'with name keyword' do
      subject { Oasis::Switcher.new(name: name) }
  
      it { expect(subject.persisted?).to be_falsey }
      it { expect(subject.valid?).to be_falsey }
      it { expect(subject.name).to eq(name) }
      it { expect(subject.id).to eq(subject.name) }
      it { expect(subject.to_param).to eq(subject.id) }
      it { expect(subject.host).to be_nil }
      it { expect(subject.object).to be_nil }
      it { expect(subject.port).to be_nil }
      it { expect(subject.managed_deployment).to be_nil }
    end
  
    context 'with port keyword' do
      subject { Oasis::Switcher.new(port: port) }
  
      it { expect(subject.persisted?).to be_falsey }
      it { expect(subject.valid?).to be_falsey }
      it { expect(subject.name).to be_nil }
      it { expect(subject.id).to be_nil }
      it { expect(subject.to_param).to be_nil }
      it { expect(subject.host).to be_nil }
      it { expect(subject.object).to be_nil }
      it { expect(subject.port).to eq(port) }
      it { expect(subject.managed_deployment).to be_nil }
    end
  
    context 'with name and port keyword' do
      subject { Oasis::Switcher.new(name: name, port: port) }
  
      it { expect(subject.persisted?).to be_falsey }
      it { expect(subject.valid?).to be_truthy }
      it { expect(subject.name).to eq(name) }
      it { expect(subject.id).to eq(subject.name) }
      it { expect(subject.to_param).to eq(subject.id) }
      it { expect(subject.host).to be_nil }
      it { expect(subject.object).to be_nil }
      it { expect(subject.port).to eq(port) }
      it { expect(subject.managed_deployment).to be_nil }
      it { expect(subject.labels).to be_empty }
      it { expect(subject.host).to be_nil }
    end

    context 'with name and port keyword' do
      subject { Oasis::Switcher.new(name: name, port: port) }
  
      it { expect(subject.persisted?).to be_falsey }
      it { expect(subject.valid?).to be_truthy }
      it { expect(subject.name).to eq(name) }
      it { expect(subject.id).to eq(subject.name) }
      it { expect(subject.host).to be_nil }
      it { expect(subject.object).to be_nil }
      it { expect(subject.port).to eq(port) }
      it { expect(subject.managed_deployment).to be_nil }
      it { expect(subject.labels).to be_empty }
    end

    context 'with **params' do
      let(:params) do
        {
          name: name, port: port
        }
      end 
      subject { Oasis::Switcher.new(**params) }
  
      it { expect(subject.persisted?).to be_falsey }
      it { expect(subject.valid?).to be_truthy }
      it { expect(subject.name).to eq(name) }
      it { expect(subject.id).to eq(subject.name) }
      it { expect(subject.host).to be_nil }
      it { expect(subject.object).to be_nil }
      it { expect(subject.port).to eq(port) }
      it { expect(subject.managed_deployment).to be_nil }
      it { expect(subject.labels).to be_empty }
    end

    context 'with object keyword' do
      subject { Oasis::Switcher.new(object: mocked_route) }

      it { expect(subject.persisted?).to be_truthy }
      it { expect(subject.valid?).to be_truthy }
      it { expect(subject.port).to eq(port) }
      it { expect(subject.name).to eq(name) }
      it { expect(subject.id).to eq(subject.name) }
      it { expect(subject.to_param).to eq(subject.id) }
      it { expect(subject.object).to eq(mocked_route) }
      it { expect(subject.labels['foo']).to eq(labels['foo']) }
      it { expect(subject.host).to eq(host) }
  
      context 'when no deployments are managed by the switcher' do
        it { expect(subject.managed_deployment_id).to be_nil }
        it { expect(subject.managed_deployment).to be_nil }
      end

      context 'when a deployment is managed by the switcher' do
        let(:managed_deployment) do
          FactoryBot.build(:deployment, labels: {
            "oasis.duke.edu/#{name}-managed" => 'true'
          })
        end
        before(:each) do
          mocked_k8s_watcher.deployment_managed_by(name, managed_deployment)
        end

        it { expect(subject.managed_deployment_id).to eq(managed_deployment.id) }
        it { expect(subject.managed_deployment).to be }
        it { expect(subject.managed_deployment.id).to eq(managed_deployment.id) }
      end
    end

    context 'with name, port, and object' do
      let(:unexpected_name) { Faker::Artist.name }
      let(:unexpected_port) { Faker::Number.number(digits: 4) }
      subject do
        Oasis::Switcher.new(
          name: unexpected_name,
          port: unexpected_port,
          object: mocked_route
        )
      end

      it 'ignores name, host, and port arguments in favor of object definitions' do
        expect(subject.name).not_to eq(unexpected_name)
        expect(subject.name).to eq(name)
        expect(subject.port).not_to eq(unexpected_port)
        expect(subject.port).to eq(port)
      end
    end
  end

  describe '.count' do
    let(:routes) { build_pair(:switchers, :object) }
    subject { described_class.count }
    context 'no routes exist' do
      it { is_expected.to eq(0) }
    end

    context 'routes exist' do
      let(:routes) { FactoryBot.build_pair(:switcher) }
      it 'returns the count of existing routes' do
        mocked_k8s_watcher.routes = routes
        is_expected.to eq(routes.length)
      end
    end
  end

  describe '.all' do
    it 'returns all routes as Oasis::Switcher objects' do
      mocked_k8s_watcher.routes = [subject]
      expect(Oasis::Switcher.all).to include(subject)
    end
  end

  describe '.find' do
    let(:id) { name }
    let(:call) { Oasis::Switcher.find(id) }

    before(:each) do
      if expected_error
        allow(mocked_k8s_watcher).to receive(:find_route)
          .with(name) do
            raise expected_error
          end
      else
        mocked_k8s_watcher.routes = [expected_switcher]
      end
    end

    context 'route with id as name exists' do
      let(:expected_switcher) { switcher }
      let(:expected_error) { nil }

      it 'returns all routes as Oasis::Switcher objects' do
        expect(call).to be_persisted
        expect(call.id).to eq(expected_switcher.id)
      end
    end

    context 'K8s::Error::Forbidden raised' do
      let(:expected_error) { K8s::Error::Forbidden.new('method', 'path', 'code', 'reason') }

      it 'raises an Oasis::SwitcherError' do
        expect {
          call
        }.to raise_error(Oasis::SwitcherError, 'action not allowed')
      end
    end

    context 'K8s::Error::NotFound raised' do
      let(:expected_error) { K8s::Error::NotFound.new('method', 'path', 'code', 'reason') }
      it 'raises an Oasis::SwitcherError' do
        expect {
          call
        }.to raise_error(Oasis::UnknownSwitcherError, "Switcher not found with id #{id}")
      end
    end
  end

  describe '#save' do
    let(:call) { subject.save }

    context 'when persisted? true' do
      subject { switcher }

      it 'returns true' do
        expect(Oasis::Switcher).not_to receive(:find)
        expect(mocked_k8s_watcher).not_to receive(:create_route)
        expect(mocked_k8s_watcher).not_to receive(:create_service)
        expect {
          expect(call).to be_truthy
        }.not_to raise_error
      end
    end

    context 'when persisted? false' do
      context 'when valid? false' do
        subject do 
          Oasis::Switcher.new
        end

        it 'returns false' do
          expect(Oasis::Switcher).not_to receive(:find)
          expect(mocked_k8s_watcher).not_to receive(:create_route)
          expect(mocked_k8s_watcher).not_to receive(:create_service)
          expect {
            expect(call).to be_falsey
          }.not_to raise_error
        end
      end

      context 'when valid? true' do
        subject do 
          Oasis::Switcher.new(name: name, host: host, port: port)
        end

        context 'when switcher with same name already exists' do
          before do
            expect(Oasis::Switcher)
              .to receive(:find)
              .with(name)
              .and_return(subject)
          end
          it 'should return false, set valid? false and not call create_route or create_service' do
            expect(mocked_k8s_watcher).not_to receive(:create_route)
            expect(mocked_k8s_watcher).not_to receive(:create_service)
            expect {
              expect(call).to be_falsey
              expect(subject.valid?).to be_falsey
              expect(subject.error).not_to be_nil
              expect(subject.error).to eq('switcher already exists with the given name')
            }.not_to raise_error
          end
        end

        context 'when switcher with name does not exist' do
          let(:expected_labels) do
            {
              'oasis.duke.edu/ingress-switcher' => 'true',
              'oasis.duke.edu/ingress-switcher-name' => name
            }
          end
          let(:expected_selector) do
            {
              'oasis.duke.edu/ingress-switcher-managed' => 'true',
              "oasis.duke.edu/#{name}-managed" => 'true'
            }
          end

          before do
            allow(Oasis::Switcher)
              .to receive(:find)
              .with(name) do
                raise Oasis::UnknownSwitcherError
            end
          end

          context 'when create_route fails' do
            let(:expected_client_error) { K8s::Error::Forbidden.new('method', 'path', 'code', 'reason') }
  
            before do
              expect(mocked_k8s_watcher).to receive(:create_route) 
                .with(
                  name: name,
                  port: port,
                  labels: expected_labels,
                  service: name
                ) do
                raise expected_client_error
              end
            end
            it 'should an invalid switcher without calling create_service' do
              expect(mocked_k8s_watcher).not_to receive(:create_service)
              expect {
                expect(call).to be_falsey
                expect(subject).not_to be_valid
                expect(subject.error).not_to be_nil
                expect(subject.error).to eq(expected_client_error.message)
              }.not_to raise_error
            end
          end
    
          context 'when create_route succeeds' do
            before do
              expect(mocked_k8s_watcher).to receive(:create_route) 
                .with(
                  name: name,
                  port: port,
                  labels: expected_labels,
                  service: name
                )
                .and_return(mocked_route)
            end  
            context 'when create_service fails' do
              let(:expected_client_error) { K8s::Error::Forbidden.new('method', 'path', 'code', 'reason') }
    
              it 'raises Oasis::Switcher::Error' do
                expect(mocked_k8s_watcher).to receive(:create_service)
                  .with(
                    name: name,
                    port: port,
                    labels: expected_labels,
                    selector: expected_selector
                  ) do
                    raise expected_client_error
                end
                expect(mocked_k8s_watcher).to receive(:delete_route).with(name: name).and_return(true)
                expect {
                  expect(call).to be_falsey
                  expect(subject).not_to be_valid
                  expect(subject.error).not_to be_nil
                  expect(subject.error).to eq(expected_client_error.message)
                }.not_to raise_error
              end
            end
    
            context 'when create_service succeeds' do
              it 'returns true, sets persisted? true ' do
                expect(mocked_k8s_watcher).to receive(:create_service)
                .with(
                  name: name,
                  port: port,
                  labels: expected_labels,
                  selector: expected_selector
                ).and_return(true)
    
                expect {
                  expect(call).to be_truthy
                  expect(subject.persisted?).to be_truthy
                  expect(subject.host).to eq(host)
                  expect(subject.object).to eq(mocked_route)
                }.not_to raise_error
              end
            end
          end
        end
      end
    end
  end

  describe '#update' do
    let(:call) { subject.update(params) }

    context 'with invalid params' do
      let(:params) do
        {
          name: Faker::Artist.name
        }
      end
      let(:expected_error) { Oasis::SwitcherError }

      it 'raises Oasis::SwitchableDeploymentError without calling any K8sWatcher methods' do
        expect {
          call
        }.to raise_error(expected_error, 'unsupported parameters')
      end
    end

    context 'with valid params' do
      let(:params) do
        {
          managed_deployment_id: managed_deployment_id
        }
      end

      context 'when managed_deployment_id is nil' do
        let(:managed_deployment_id) { nil }

        context 'and a deployment is not currently managed by the switcher' do
          does_not_unset_or_set
          it { 
              expect {
                expect(call).to be_truthy
                }.not_to raise_error
            }
        end

        context 'and a deployment is currently managed by the switcher' do
          let(:managed_deployment) { FactoryBot.build(:deployment) }

          has_managed_deployment
          context 'and unset_managed_deployment raises Oasis::SwitchableDeploymentError' do
            raises_unset_managed_deployment_error
            it { expect { subject.update(params) }.to raise_error(unset_error) }
          end

          context 'and unset_managed_deployment is successful' do
            before(:each) do
              is_expected.to receive(:unset_managed_deployment)
                .and_return(true) 
              is_expected.not_to receive(:set_managed_deployment)
            end
            it { 
              expect {
                expect(call).to be_truthy
              }.not_to raise_error
            }
          end
        end
      end

      context 'when managed_deployment_id is blank' do
        let(:managed_deployment_id) { "" }

        context 'and a deployment is not currently managed by the switcher' do
          does_not_unset_or_set
          it { 
            expect {
              expect(call).to be_truthy
              }.not_to raise_error
          }
        end

        context 'and a deployment is currently managed by the switcher' do
          let(:managed_deployment) { FactoryBot.build(:deployment) }
          has_managed_deployment
          context 'and unset_managed_deployment raises an error' do
            raises_unset_managed_deployment_error
            it { expect { call }.to raise_error(unset_error) }
          end

          context 'and unset_managed_deployment is successful' do
            before(:each) do
              is_expected.to receive(:unset_managed_deployment)
                .and_return(true) 
              is_expected.not_to receive(:set_managed_deployment)
            end
            it { 
              expect {
                expect(call).to be_truthy
              }.not_to raise_error
            }
          end
        end
      end

      context 'with managed_deployment_id is not nil or blank' do
        let(:managed_deployment_id) { Faker::Artist.name }

        context 'and a deployment is not currently managed by the switcher' do
          context 'and set_managed_deployment raises error' do
            let(:set_error) { Oasis::SwitchableDeploymentError.new 'error' }
  
            before(:each) do
              is_expected.not_to receive(:unset_managed_deployment) 
              is_expected.to receive(:set_managed_deployment).with(managed_deployment_id) do
                raise set_error
              end
            end
            it { expect { call }.to raise_error(set_error) }
          end
  
          context 'and set_managed_deployment is successful' do
            before(:each) do
              is_expected.not_to receive(:unset_managed_deployment) 
              is_expected.to receive(:set_managed_deployment).with(managed_deployment_id)
                .and_return(true)
            end
            it { 
              expect {
                expect(call).to be_truthy
              }.not_to raise_error
            }
          end
  
          context 'and a deployment is currently managed by the switcher' do
            context 'and is the requested update managed_deployment_id' do
              let(:managed_deployment) { FactoryBot.build(:deployment, name: managed_deployment_id) }

              has_managed_deployment
              does_not_unset_or_set
              it { 
                expect {
                  expect(call).to be_truthy
                }.not_to raise_error
              }
            end

            context 'and is not the requested update managed_deployment_id' do
              let(:managed_deployment) { FactoryBot.build(:deployment) }

              has_managed_deployment
              context 'and unset_managed_deployment raises an error' do
                raises_unset_managed_deployment_error
                it { expect { call }.to raise_error(unset_error) }
              end

              context 'and unset_managed_deployment is successfull' do
                context 'and set_managed_deployment raises error' do
                  let(:set_error) { Oasis::SwitchableDeploymentError.new 'error' }
        
                  before(:each) do
                    is_expected.to receive(:unset_managed_deployment).and_return(true) 
                    is_expected.to receive(:set_managed_deployment).with(managed_deployment_id) do
                      raise set_error
                    end
                  end
                  it { expect { call }.to raise_error(set_error) }
                end
      
                context 'and set_managed_deployment is successful' do
                  before(:each) do
                    is_expected.to receive(:unset_managed_deployment)
                      .and_return(true) 
                    is_expected.to receive(:set_managed_deployment).with(managed_deployment_id)
                      .and_return(true)
                  end
                  it { 
                    expect {
                      expect(call).to be_truthy
                    }.not_to raise_error
                  }
                end
              end
            end
          end
        end
      end
    end
  end

  describe '#destroy' do
    let(:call) { subject.destroy }

    context 'when persisted? false' do
      subject do 
        Oasis::Switcher.new(name: name, host: host, port: port)
      end

      it 'returns true without making any k8s calls' do
        expect(mocked_k8s_watcher).not_to receive(:delete_service)
        expect(mocked_k8s_watcher).not_to receive(:delete_route)
        expect {
          expect(call).to be_truthy
        }.not_to raise_error
      end
    end

    context 'when persisted? true' do
      subject { switcher }

      context 'when delete_service raises K8s::Error' do
        let(:expected_client_error) { K8s::Error::Forbidden.new('method', 'path', 'code', 'reason') }

        it 'raises an Oasis::SwitcherError' do
          expect(mocked_k8s_watcher).to receive(:delete_service)
            .with(name: name) do
              raise expected_client_error
          end
          expect(mocked_k8s_watcher).not_to receive(:delete_route)

          expect {
            call
          }.to raise_error(Oasis::SwitcherError)
        end
      end

      context 'when delete_route raises K8s::Error' do
        let(:expected_client_error) { K8s::Error::Forbidden.new('method', 'path', 'code', 'reason') }

        it 'raises an Oasis::SwitcherError' do
          expect(mocked_k8s_watcher).to receive(:delete_service)
            .with(name: name)
            .and_return(true)
          expect(mocked_k8s_watcher).to receive(:delete_route)
            .with(name: name) do
              raise expected_client_error
          end

          expect {
            call
          }.to raise_error(Oasis::SwitcherError)
        end
      end

      context 'when K8s::Error is not raised' do
        before do
          allow(mocked_k8s_watcher).to receive(:delete_service)
            .with(name: name)
            .and_return(true)
          allow(mocked_k8s_watcher).to receive(:delete_route)
            .with(name: name).and_return(true)
        end
        context 'with managed_deployment' do
          let(:managed_deployment) { FactoryBot.build(:deployment) }
          has_managed_deployment

          context 'and unset_managed_deployment raises Oasis::SwitchableDeploymentError' do
            raises_unset_managed_deployment_error
            it { expect { call }.to raise_error(Oasis::SwitcherError) }
          end

          context 'and unset_managed_deployment is successful' do
            before(:each) do
              is_expected.to receive(:unset_managed_deployment)
                .and_return(true)
            end
            it 'returns true' do
              expect {
                expect(call).to be_truthy
              }.not_to raise_error
            end
        end

        context 'without managed_deployment' do
          it 'returns true' do
            is_expected.to receive(:unset_managed_deployment)
              .and_return(true)
            expect {
              expect(call).to be_truthy
            }.not_to raise_error
          end
          end
        end
      end
    end
  end

  describe '.create' do
    let(:params) do
      {
        port: port,
        name: name
      }
    end
    let(:mocked_switcher) { Oasis::Switcher.new }

    before do
      expect(Oasis::Switcher).to receive(:new)
        .with(params)
        .and_return(mocked_switcher)
      allow(mocked_switcher).to receive(:save).and_return(is_saved)
      allow(mocked_switcher).to receive(:valid?).and_return(is_valid)
      allow(mocked_switcher).to receive(:error).and_return(expected_error)
      allow(mocked_switcher).to receive(:persisted?).and_return(is_persisted)
      allow(mocked_switcher).to receive(:object).and_return(expected_object)
    end

    context 'save succeeds' do
      let(:is_saved) { true }
      let(:is_valid) { true }
      let(:is_persisted) { true }
      let(:expected_error) { nil }
      let(:expected_object) { mocked_route }

      it 'returns the valid, persisted object' do
        new_switcher = Oasis::Switcher.create(params)
        expect(new_switcher).to be_valid
        expect(new_switcher).to be_persisted
        expect(new_switcher.error).to be_nil
        expect(new_switcher.object).to eq(expected_object)
      end
    end

    context 'save fails' do
      let(:is_saved) { false }
      let(:is_valid) { false }
      let(:is_persisted) { false }
      let(:expected_error) { 'some error' }
      let(:expected_object) { nil }

      it 'returns the invalid, unpersisted object with error' do
        new_switcher = Oasis::Switcher.create(params)
        expect(new_switcher).not_to be_valid
        expect(new_switcher).not_to be_persisted
        expect(new_switcher.error).to eq(expected_error)
        expect(new_switcher.object).to eq(expected_object)
      end
    end
  end

  # 'private' public testing methods to make testing easier
  describe '#unset_managed_deployment' do
    let(:call) { subject.unset_managed_deployment }

    context 'no deployment is managed by the switcher' do
      it {
        expect {
          expect(call).to be_truthy
        }.not_to raise_error
      }
    end

    context 'deployment is managed by the switcher' do
      let(:managed_deployment) { FactoryBot.build(:deployment) }

      context 'and deployment update raises Oasis::SwitchableDeploymentError' do
        let(:deployment_update_error) { Oasis::SwitchableDeploymentError.new('error') }

        has_managed_deployment
        before(:each) do
          expect(managed_deployment).to receive(:update)
            .with({labels: {subject.managing_label => nil }}) do
              raise deployment_update_error
            end
        end      
        it {
          expect {
            call
          }.to raise_error(deployment_update_error)
        }
      end

      context 'update is successful' do
        has_managed_deployment
        before(:each) do
          expect(managed_deployment).to receive(:update)
            .with({labels: {subject.managing_label => nil }})
            .and_return(true)
        end
        it { 
          expect {
            expect(call).to be_truthy
          }.not_to raise_error
        }
      end
    end
  end

  describe '#set_managed_deployment' do
    let(:managed_deployment_id) { Faker::Artist.name }
    let(:managed_deployment) { FactoryBot.build(:deployment) }
    let(:call) {subject.set_managed_deployment(managed_deployment_id)}

    context 'when deployment find raises Oasis::UnknwonSwitchableDeploymentError' do
      let(:deployment_find_error) { Oasis::UnknownSwitchableDeploymentError.new('object not found') }

      before(:each) do  
        expect(Oasis::SwitchableDeployment).to receive(:find)
          .with(managed_deployment_id) do
            raise deployment_find_error
        end
        expect(managed_deployment).not_to receive(:update)
      end
      it { 
        expect {
          call
        }.to raise_error(deployment_find_error)
      }
    end

    context 'when deployment find raises Oasis::SwitchableDeploymentError' do
      let(:deployment_find_error) { Oasis::SwitchableDeploymentError.new('error') }

      before(:each) do  
        expect(Oasis::SwitchableDeployment).to receive(:find)
          .with(managed_deployment_id) do
            raise deployment_find_error
        end
        expect(managed_deployment).not_to receive(:update)
      end
      it { 
        expect {
          call
        }.to raise_error(deployment_find_error)
      }
    end

    context 'when deployment update raises Oasis::SwitchableDeploymentError' do
      let(:deployment_update_error) { Oasis::SwitchableDeploymentError.new('error') }

      before(:each) do
        expect(Oasis::SwitchableDeployment).to receive(:find)
          .with(managed_deployment_id)
          .and_return(managed_deployment)

        expect(managed_deployment).to receive(:update)
          .with({labels: {subject.managing_label => 'true' }}) do
            raise deployment_update_error
          end
      end
      it {
        expect {
          call
        }.to raise_error(deployment_update_error)
      }
    end

    context 'when deployment update is successful' do
      before(:each) do
        expect(Oasis::SwitchableDeployment).to receive(:find)
          .with(managed_deployment_id)
          .and_return(managed_deployment)

        expect(managed_deployment).to receive(:update)
          .with({labels: {subject.managing_label => 'true' }})
          .and_return(true)
      end
      it { 
        expect {
          expect(call).to be_truthy
        }.not_to raise_error
      }
    end
  end
end
