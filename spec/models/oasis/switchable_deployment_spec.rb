require 'rails_helper'

RSpec.describe Oasis::SwitchableDeployment do
  let(:name) { Faker::Artist.name }
  let(:application_name) { Faker::Ancient.god }
  let(:application_environment) { Faker::Dessert.flavor }
  let(:labels) do
    {
      :'oasis.duke.edu/application-name' => application_name,
      :'oasis.duke.edu/application-environment' => application_environment,
      :'foo' => 'bar'
    }
  end
  let(:mocked_deployment) do
    {
      'metadata' => {
        'name' => name,
        'labels' => labels
      }
    }
  end
  subject { Oasis::SwitchableDeployment.new mocked_deployment }
  include_context 'with mocked k8s_watcher'

  it { is_expected.to be_an ActiveModel::Model }
  it { is_expected.to respond_to(:id) }
  it { expect(subject.id).to eq(subject.name) }
  it { expect(subject.persisted?).to be_truthy }
  it { expect(described_class).to respond_to(:find).with(1).argument }
  it { expect(described_class).not_to respond_to(:find).with(0).arguments }
  it { is_expected.to respond_to(:update).with(1).argument }
  it { is_expected.not_to respond_to(:update).with(0).arguments }
  it { is_expected.to respond_to(:destroy).with(0).arguments }

  it { is_expected.to be_an ActiveModel::Serializers::JSON }
  it { expect(subject.attributes).to eq({'id' => nil, 'name' => nil}) }

  it { is_expected.to respond_to(:name) }
  it { is_expected.to respond_to(:object) }
  it { expect(subject.object).to eq(mocked_deployment) }
  it { expect(subject.name).to eq(name) }
  it { expect(subject.labels['foo']).to eq(labels['foo']) }
  it { is_expected.to respond_to(:persisted?) }
  it { is_expected.to respond_to(:error) }

  it { expect(described_class).to respond_to(:all) }
  describe '.all' do
    it 'returns all routes as Oasis::Switcher objects' do
      mocked_k8s_watcher.deployment_managed_by(Faker::Artist.name, subject)
      expect(Oasis::SwitchableDeployment.all).to include(subject)
    end
  end

  it { expect(described_class).to respond_to(:create).with(1).argument }
  describe '.create' do
    let(:param_labels) { "oasis.duke.edu/application-name=#{application_name},oasis.duke.edu/application-environment=#{application_environment}" }
    let(:params) do
      {
        labels: param_labels
      }
    end

    context 'when no deployments exist with the selector provided' do
      it 'raises Oasis::UnknownSwitchableDeployment error' do
        expect {
          Oasis::SwitchableDeployment.create(params)
        }.to raise_error(Oasis::UnknownSwitchableDeploymentError)
      end
    end

    context 'when deployments exist with the selector provided' do
      let(:existing_deployments) { FactoryBot.build_pair(:deployment, labels: labels) }
      let(:expected_selector) do
        {
          "oasis.duke.edu/application-name" => application_name,
          "oasis.duke.edu/application-environment" => application_environment
        }
      end
      let(:expected_labeled_deployments) do
        {
          expected_selector => existing_deployments
        }
      end
      let(:ingress_switcher_managed_label) do
        {
          labels: {
            Oasis::Switcher.managed_deployment_label => 'true'
          }
        }
      end

      it 'gets all pods with the given labels, makes Oasis::SwitchableDeployment objects from them, and updates them to make them ingress-switcher-managed, and returns Oasis::SwitchableDeployment objects' do
        mocked_k8s_watcher.labeled_deployments = expected_labeled_deployments
        existing_deployments.each do |deployment|
          expect(deployment).to receive(:update)
            .with(ingress_switcher_managed_label) do |new_labels|
              deployment.labels.merge!(new_labels[:labels])
              true
            end
        end
        deployments = Oasis::SwitchableDeployment.create(params)
        expect(deployments).not_to be_empty
        deployments.each do |deployment|
          expect(deployment.labels[Oasis::Switcher.managed_deployment_label]).to eq('true')
        end
      end
    end
  end

  describe '#find' do
    let(:id) { Faker::Artist.name }
    let(:call) { Oasis::SwitchableDeployment.find(id) }

    context 'K8s::Error::Forbidden raised' do
      let(:expected_error) { K8s::Error::Forbidden.new('method', 'path', 'code', 'reason') }

      before(:each) do
        expect(mocked_k8s_watcher).to receive(:find_deployment).with(id) do
          raise expected_error
        end
      end  
      it 'raises an Oasis::SwitchableDeploymentError' do
        expect {
          call
        }.to raise_error(Oasis::SwitchableDeploymentError, 'action not allowed')
      end
    end

    context 'K8s::Error::NotFound raised' do
      let(:expected_error) { K8s::Error::NotFound.new('method', 'path', 'code', 'reason') }

      before(:each) do
        expect(mocked_k8s_watcher).to receive(:find_deployment).with(id) do
          raise expected_error
        end
      end  
      it 'raises an Oasis::SwitchableDeploymentError' do
        expect {
          call
        }.to raise_error(Oasis::UnknownSwitchableDeploymentError, "Deployment not found with id #{id}")
      end
    end

    context 'when deployment exists with the id' do
      let(:expected_deployment) { FactoryBot.build(:deployment, name: id) }
      let(:expected_error) { nil }

      before(:each) do
        mocked_k8s_watcher.all_deployments = [expected_deployment]
      end  
      it 'returns the expected Oasis::SwitchableDeployment object' do
        expect(call).to eq(expected_deployment)
      end
    end
  end

  describe '#update' do
    let(:new_labels) { Hash[*Faker::Lorem.words(number: 2)] }
    let(:expected_selector) do
      labels.slice(
        :'oasis.duke.edu/application-name',
        :'oasis.duke.edu/application-environment'
      )
    end
    let(:expected_pod_id) { "#{name}-#{Faker::Crypto.md5}" }
    let(:expected_pod) do
      {
        'metadata' => {
          'name' => expected_pod_id,
          'labels' => expected_selector
        }
      }
    end
    let(:expected_pods) { [expected_pod] }
    let(:call) { subject.update(params) }
    let(:patched_deployment_object) do
      {
        'metadata' => {
          'name' => name,
          'labels' => labels.merge(new_labels)
        }
      }  
    end

    context 'invalid parameters' do
      let(:params) do
        {
          name: Faker::Artist.name
        }
      end
      let(:expected_error) { Oasis::SwitchableDeploymentError }

      it 'raises Oasis::SwitchableDeploymentError without calling any K8sWatcher methods' do
        expect(K8sWatcher).not_to receive(:instance)
        expect {
          call
        }.to raise_error(expected_error, 'unsupported update')
      end
    end

    context 'valid parameters' do
      let(:params) do
        {
          labels: new_labels
        }
      end

      context 'labels already exist' do
        let(:new_labels) { labels.slice(:"foo") }

        it 'returns true without calling any K8sWatcher methods' do
          expect(K8sWatcher).not_to receive(:instance)
          expect {
            expect(call).to be_truthy
          }.not_to raise_error
        end
      end

      context 'K8s::Error::Forbidden raised' do
        context 'deployment label update' do
          it 'raises an Oasis::SwitchableDeploymentError' do
            expect(mocked_k8s_watcher).to receive(:patch_deployment_labels).with(name, new_labels) do
              raise K8s::Error::Forbidden.new('method', 'path', 'code', 'reason')
            end
            expect(mocked_k8s_watcher).not_to receive(:get_pods_with_labels)
            expect(mocked_k8s_watcher).not_to receive(:patch_pod_labels)
            expect {
              call
            }.to raise_error(Oasis::SwitchableDeploymentError, 'action not allowed')
          end
        end

        context 'pod label update' do
          context 'get_pods_with_labels' do
            it 'raises an Oasis::SwitchableDeploymentError' do
              expect(mocked_k8s_watcher).to receive(:patch_deployment_labels).with(name, new_labels)
                .and_return(patched_deployment_object)
              expect(mocked_k8s_watcher).to receive(:get_pods_with_labels).with(expected_selector) do
                raise K8s::Error::Forbidden.new('method', 'path', 'code', 'reason')
              end
              expect(mocked_k8s_watcher).not_to receive(:patch_pod_labels)
              expect {
                call
              }.to raise_error(Oasis::SwitchableDeploymentError, 'action not allowed')
            end
          end

          context 'patch_pod_labels' do
            it 'raises an Oasis::SwitchableDeploymentError' do
              expect(mocked_k8s_watcher).to receive(:patch_deployment_labels).with(name, new_labels)
                .and_return(patched_deployment_object)
              mocked_k8s_watcher.labeled_pods = { expected_selector => expected_pods }              
              expect(mocked_k8s_watcher).to receive(:patch_pod_labels).with(expected_pod_id, new_labels) do
                raise K8s::Error::Forbidden.new('method', 'path', 'code', 'reason')
              end
              expect {
                call
              }.to raise_error(Oasis::SwitchableDeploymentError, 'action not allowed')
            end
          end
        end
      end

      context 'K8s::Error::NotFound raised' do
        context 'deployment label update' do
          it 'raises an Oasis::SwitchableDeploymentError' do
            expect(mocked_k8s_watcher).to receive(:patch_deployment_labels).with(name, new_labels) do
              raise K8s::Error::NotFound.new('method', 'path', 'code', 'reason')
            end
            expect(mocked_k8s_watcher).not_to receive(:get_pods_with_labels)
            expect(mocked_k8s_watcher).not_to receive(:patch_pod_labels)
            expect {
              call
            }.to raise_error(Oasis::UnknownSwitchableDeploymentError, 'object not found')
          end
        end

        context 'get_pods_with_labels' do
          it 'raises an Oasis::SwitchableDeploymentError' do
            expect(mocked_k8s_watcher).to receive(:patch_deployment_labels).with(name, new_labels)
              .and_return(patched_deployment_object)
            expect(mocked_k8s_watcher).to receive(:get_pods_with_labels).with(expected_selector) do
              raise K8s::Error::NotFound.new('method', 'path', 'code', 'reason')
            end
            expect(mocked_k8s_watcher).not_to receive(:patch_pod_labels)
            expect {
              call
            }.to raise_error(Oasis::UnknownSwitchableDeploymentError, 'object not found')
          end
        end

        context 'patch_pod_labels' do
          it 'raises an Oasis::SwitchableDeploymentError' do
            expect(mocked_k8s_watcher).to receive(:patch_deployment_labels).with(name, new_labels)
              .and_return(patched_deployment_object)
            mocked_k8s_watcher.labeled_pods = { expected_selector => expected_pods }              
            expect(mocked_k8s_watcher).to receive(:patch_pod_labels).with(expected_pod_id, new_labels) do
              raise K8s::Error::NotFound.new('method', 'path', 'code', 'reason')
            end
            expect {
              call
            }.to raise_error(Oasis::UnknownSwitchableDeploymentError, 'object not found')
          end
        end
      end

      context 'K8s::Error::MethodNotAllowed raised' do
        context 'deployment label update' do
          it 'raises an Oasis::SwitchableDeploymentError' do
            expect(mocked_k8s_watcher).to receive(:patch_deployment_labels).with(name, new_labels) do
              raise K8s::Error::MethodNotAllowed.new('method','path','code','reason')
            end
            expect(mocked_k8s_watcher).not_to receive(:get_pods_with_labels)
            expect(mocked_k8s_watcher).not_to receive(:patch_pod_labels)
            expect {
              call
            }.to raise_error(Oasis::SwitchableDeploymentError)
          end
        end

        context 'pod_label_update' do
          context 'get_pods_with_labels' do
            it 'raises an Oasis::SwitchableDeploymentError' do
              expect(mocked_k8s_watcher).to receive(:patch_deployment_labels).with(name, new_labels)
                .and_return(patched_deployment_object)
              expect(mocked_k8s_watcher).to receive(:get_pods_with_labels).with(expected_selector) do
                raise K8s::Error::MethodNotAllowed.new('method','path','code','reason')
              end
              expect(mocked_k8s_watcher).not_to receive(:patch_pod_labels)
              expect {
                call
              }.to raise_error(Oasis::SwitchableDeploymentError)
            end
          end

          context 'patch_pod_labels' do
            it 'raises an Oasis::SwitchableDeploymentError' do
              expect(mocked_k8s_watcher).to receive(:patch_deployment_labels).with(name, new_labels)
                .and_return(patched_deployment_object)
              mocked_k8s_watcher.labeled_pods = { expected_selector => expected_pods }
              expect(mocked_k8s_watcher).to receive(:patch_pod_labels).with(expected_pod_id, new_labels) do
                raise K8s::Error::MethodNotAllowed.new('method','path','code','reason')
              end
              expect {
                call
              }.to raise_error(Oasis::SwitchableDeploymentError)
            end
          end
        end
      end
    
      context 'K8s::Error::Invalid raised' do
        context 'deployment label update' do
          it 'raises an Oasis::SwitchableDeploymentError' do
            expect(mocked_k8s_watcher).to receive(:patch_deployment_labels).with(name, new_labels) do
              raise K8s::Error::Invalid.new('method', 'path', 'code', 'reason')
            end
            expect(mocked_k8s_watcher).not_to receive(:get_pods_with_labels)
            expect(mocked_k8s_watcher).not_to receive(:patch_pod_labels)
            expect {
              call
            }.to raise_error(Oasis::SwitchableDeploymentError)
          end
        end

        context 'pod_label_update' do
          context 'get_pods_with_labels' do
            it 'raises an Oasis::SwitchableDeploymentError' do
              expect(mocked_k8s_watcher).to receive(:patch_deployment_labels).with(name, new_labels)
                .and_return(patched_deployment_object)
              expect(mocked_k8s_watcher).to receive(:get_pods_with_labels).with(expected_selector) do
                raise K8s::Error::MethodNotAllowed.new('method','path','code','reason')
              end
              expect(mocked_k8s_watcher).not_to receive(:patch_pod_labels)
              expect {
                call
              }.to raise_error(Oasis::SwitchableDeploymentError)
            end
          end

          context 'patch_pod_labels' do
            it 'raises an Oasis::SwitchableDeploymentError' do
              expect(mocked_k8s_watcher).to receive(:patch_deployment_labels).with(name, new_labels)
                .and_return(patched_deployment_object)
              mocked_k8s_watcher.labeled_pods = { expected_selector => expected_pods }
              expect(mocked_k8s_watcher).to receive(:patch_pod_labels).with(expected_pod_id, new_labels) do
                raise K8s::Error::Invalid.new('method', 'path', 'code', 'reason')
              end
              expect {
                call
              }.to raise_error(Oasis::SwitchableDeploymentError)
            end
          end
        end
      end
    
      context 'success' do
        it 'uses K8sWatcher to update the deployment pod labels and returns true' do
          allow(mocked_k8s_watcher).to receive(:patch_deployment_labels).with(name, new_labels)
            .and_return(patched_deployment_object)
          mocked_k8s_watcher.labeled_pods = { expected_selector => expected_pods }
          expect(mocked_k8s_watcher).to receive(:patch_deployment_labels).with(name, new_labels).and_return(patched_deployment_object)
          expect(mocked_k8s_watcher).to receive(:patch_pod_labels).with(expected_pod_id, new_labels)
            .and_return(expected_pod)
          expect {
            expect(call).to be_truthy
          }.not_to raise_error
          expect(subject.labels).to have_key(new_labels.keys.first)
          expect(subject.labels[new_labels.keys.first]).to eq(new_labels.values.first)
        end
      end
    end
  end

  describe '#destroy' do
    let(:expected_labels) do
      {
        Oasis::Switcher.managed_deployment_label => nil
      }
    end
    it 'calls #update to remove Oasis::Switcher.managed_deployment_label' do
      is_expected.to receive(:update).with({labels: expected_labels})
      subject.destroy
    end
  end
end
