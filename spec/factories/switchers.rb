FactoryBot.define do
  factory :switcher, class: Oasis::Switcher do
    name { Faker::Artist.name }
    id { name }
    labels { {'foo' => 'bar'} }
    host { Faker::Internet.url }
    port { Faker::Number.number(digits: 4) }

    trait :object do
      initialize_with do
        new(object: {
          'metadata' => {
            'name' => attributes[:name],
            'labels' => attributes[:labels]
          }, 
          'spec' => {
            'host' => attributes[:host],
            'port' => {
              'targetPort' => attributes[:port]
            }
          }
        })
      end
    end
  end
end
