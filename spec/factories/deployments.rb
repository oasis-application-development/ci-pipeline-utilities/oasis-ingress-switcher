FactoryBot.define do
  factory :deployment, class: Oasis::SwitchableDeployment do
    name { Faker::Artist.name }
    id { name }
    labels { {'foo' => 'bar'} }

    initialize_with { new({'metadata' => attributes.stringify_keys}) }
  end
end
