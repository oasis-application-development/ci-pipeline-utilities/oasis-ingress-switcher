class DeploymentsController < ApplicationController
  def create
    respond_to do |format|
      begin
        @deployments = Oasis::SwitchableDeployment.create(**create_deployment_params.to_h.symbolize_keys)
      rescue Oasis::SwitchableDeploymentError => e
        @deployment_creation_error = e.message
        logger.error(@deployment_creation_error)
      rescue Oasis::UnknownSwitchableDeploymentError => e
        @deployment_creation_error = e.message
        logger.error(@deployment_creation_error)
      end
      format.turbo_stream
    end
  end

  def destroy
    respond_to do |format|
      begin
        @deployment = K8sWatcher.instance.find_deployment(params[:id])
        @deployment.destroy
      rescue Oasis::SwitchableDeploymentError => e
        @deployment_creation_error = e.message
        logger.error(@deployment_creation_error)
      rescue Oasis::UnknownSwitchableDeploymentError => e
        @deployment_creation_error = e.message
        logger.error(@deployment_creation_error)
      end
      format.turbo_stream
    end
  end

  private

  def create_deployment_params = params.require(:oasis_switchable_deployment).permit(%i[labels])
end
