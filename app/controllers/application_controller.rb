# frozen_string_literal: true

class ApplicationController < ActionController::Base #:nodoc:
  protect_from_forgery with: :exception
  include K8sWatcherHelper

  rescue_from Oasis::UnknownSwitcherError, with: :record_not_found
  rescue_from Oasis::SwitcherError, with: :action_not_allowed
  rescue_from Oasis::UnknownSwitchableDeploymentError, with: :record_not_found
  rescue_from Oasis::SwitchableDeploymentError, with: :action_not_allowed

  private
  def record_not_found(error)
    render plain: "#{error.message}", status: :not_found
  end

  def action_not_allowed(error)
    render plain: "#{error.message}", status: :unprocessable_entity
  end
end
