class SwitchersController < ApplicationController
  before_action :set_switcher, only: %i[show update destroy]

  def index
    @switchers = Oasis::Switcher.all
    get_deployments    
  end

  # get /switchers/name
  def show;end

  # PATCH/PUT /switchers/name
  def update
    manage_unmanaged_deployment_list
    respond_to do |format|
      if @switcher.update(update_switcher_params)
        format.turbo_stream
        format.html { redirect_to oasis_switcher_url(@switcher), notice: 'switcher was successfully updated.' }
      else
        format.turbo_stream { render @switcher, status: :unprocessable_entity }
        format.html { render @switcher, status: :unprocessable_entity }
      end
    end
  end

  # POST /switchers
  def create
    @switcher = Oasis::Switcher.new(**create_switcher_params.to_h.symbolize_keys)
    respond_to do |format|
      if @switcher.save
        format.turbo_stream
        format.html { redirect_to oasis_switcher_url(@switcher), notice: 'Switcher was successfully created.' }
      else
        format.turbo_stream { render :show, status: :unprocessable_entity }
        format.html { render :show, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    respond_to do |format|
      @switcher.destroy
      format.turbo_stream
      format.html { redirect_to oasis_switchers_url }
    end
  end

  private

  def get_deployments
    @deployment_names = []
    @unmanaged_deployments = Oasis::SwitchableDeployment.all.reject do |m|
      @deployment_names << m.name
      @switchers.any? do |switcher|
        switcher.managed_deployment_id == m.id
      end
    end
  end

  def set_switcher
    @switcher = Oasis::Switcher.find(params[:id])
  end

  def manage_unmanaged_deployment_list
    @remove_unmanaged_deployment_id = update_switcher_params.to_h.symbolize_keys[:managed_deployment_id]
    @add_unmanaged_deployment = @switcher.managed_deployment if @switcher.managed_deployment_id != @remove_unmanaged_deployment_id
  end

  def update_switcher_params = params.require(:oasis_switcher).permit(%i[managed_deployment_id])
  def create_switcher_params = params.require(:oasis_switcher).permit(%i[name port])
end
