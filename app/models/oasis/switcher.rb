module Oasis
  class Switcher
    include ActiveModel::Model
    include ActiveModel::Serializers::JSON

    attr_accessor :id, :port, :object, :name, :labels, :host, :managed_deployment, :managed_deployment_id
    attr_reader :error
  
    def initialize(name:nil, host:nil, port:nil, object:nil)
      @error = nil
      if object
        @object = object
        @persisted = true
        @name = object['metadata']['name']
        @id = @name
        @labels = object['metadata']['labels'].to_h
        @host = object['spec']['host']
        @port = object['spec']['port']['targetPort']
        self.load_managed_deployment
      else
        @id = name
        @name = name
        @host = host
        @port = port
        @labels = {}
        @managed_deployment = nil
        @persisted = false
      end
      params = {
        id: @id,
        name: @name,
        host: @host,
        port: @port,
        managed_deployment_id: @managed_deployment_id
      }
      super(params)
    end

    def attributes
      {'name' => nil, 'host' => nil, 'port' => nil, 'managed_deployment_id' => nil, 'managed_deployment' => nil}
    end

    class << self
      def all = K8sWatcher.instance.list_routes

      def find(id)
        begin
          return K8sWatcher.instance.find_route(id)
        rescue K8s::Error::Forbidden
          raise Oasis::SwitcherError, "action not allowed"
        rescue K8s::Error::NotFound
          raise Oasis::UnknownSwitcherError, "Switcher not found with id #{id}"
        end
      end

      def create(params)
        params = params.to_h.symbolize_keys
        new_switcher = new(
          name: params[:name],
          port: params[:port]
        )
        new_switcher.save
        new_switcher
      end

      def count = K8sWatcher.instance.list_routes.length
    end

    def to_param = @id

    def persisted? = @persisted

    def valid?
      @name && @port && @error.nil?
    end

    def save
      return true if persisted?

      return false unless valid?

      begin
        existing_switcher = Oasis::Switcher.find(name)
        @error = 'switcher already exists with the given name'
        return false
      rescue Oasis::UnknownSwitcherError
        switcher_labels = {
          'oasis.duke.edu/ingress-switcher' => 'true',
          'oasis.duke.edu/ingress-switcher-name' => name
        }
    
        k8s = K8sWatcher.instance
        begin  
          switcher_object = k8s.create_route(
            name: name,
            port: port,
            labels: switcher_labels,
            service: name
          )
        rescue StandardError => e
          @error = e.message
          return false
        end

        switcher_selector = {
          'oasis.duke.edu/ingress-switcher-managed' => 'true',
          "oasis.duke.edu/#{name}-managed" => 'true'
        }
    
        begin
          k8s.create_service(
            name: name,
            port: port,
            labels: switcher_labels,
            selector: switcher_selector
          )
        rescue StandardError => e
          begin
            k8s.delete_route(name: @name)
          rescue StandardError
          end
          @error = e.message
          return false
        end
        @object = switcher_object
        @host = switcher_object['spec']['host']
        @persisted = true
        return true
      end
    end

    def update(params)
      params = params.to_h.symbolize_keys
      raise Oasis::SwitcherError.new('unsupported parameters') unless params.keys == [:managed_deployment_id]

      managed_deployment_id = params[:managed_deployment_id]
      return true if (managed_deployment_id.nil? || managed_deployment_id.blank?) && @managed_deployment_id.nil?

      return true if @managed_deployment && @managed_deployment.id == managed_deployment_id

      if @managed_deployment
        success = self.unset_managed_deployment 
        return success if (managed_deployment_id.nil? || managed_deployment_id.blank?)
      end
      return self.set_managed_deployment(managed_deployment_id)
    end
  
    def destroy
      return true unless persisted?

      begin
        self.unset_managed_deployment
        k8s = K8sWatcher.instance
        k8s.delete_service(name: @name)
        k8s.delete_route(name: @name)
      rescue StandardError => e
        raise Oasis::SwitcherError.new(e.message)
      end
    end

    def managing_label = "oasis.duke.edu/#{@name}-managed"

    def self.managed_deployment_label = 'oasis.duke.edu/ingress-switcher-managed'

    # these are publicly available for testing, but should not be used outside of testing
    def unset_managed_deployment
      return true unless @managed_deployment

      if @managed_deployment.update({labels: { self.managing_label => nil }})
        @managed_deployment_id = nil
        @managed_deployment = nil
        return true
      end
      return false
    end

    def set_managed_deployment(new_managed_deployment_id)
      @managed_deployment = Oasis::SwitchableDeployment.find(new_managed_deployment_id)
      if @managed_deployment.update({labels: { managing_label => 'true' }})
        @managed_deployment_id = managed_deployment_id
        return true
      else
        @error = @managed_deployment.error
        return false
      end
    end

    private

    def load_managed_deployment
      @managed_deployment = K8sWatcher.instance.get_deployment_managed_by(@id)
      if @managed_deployment
        @managed_deployment_id = @managed_deployment.id
      end
    end
  end
end
