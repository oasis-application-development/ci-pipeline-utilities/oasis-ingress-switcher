# frozen_string_literal: true

module Oasis
  class UnknownSwitchableDeploymentError < StandardError #:nodoc:
  end
end