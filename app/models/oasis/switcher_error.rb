# frozen_string_literal: true

module Oasis
  class SwitcherError < StandardError #:nodoc:
  end
end