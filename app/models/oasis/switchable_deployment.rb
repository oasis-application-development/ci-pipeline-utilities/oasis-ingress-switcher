module Oasis
  class SwitchableDeployment
    include ActiveModel::Model
    include ActiveModel::Serializers::JSON

    attr_accessor :id, :object, :name, :labels
    attr_reader :error
  
    def initialize(object)
      set_with_object(object)
      params = {
        id: @id,
        name: @name
      }
      super(params)
    end

    def attributes
      {'id' => nil, 'name' => nil}
    end

    def self.all = K8sWatcher.instance.list_managed_deployments

    def self.find(id)
      begin
        return K8sWatcher.instance.find_deployment(id)
      rescue K8s::Error::Forbidden
        raise Oasis::SwitchableDeploymentError, "action not allowed"
      rescue K8s::Error::NotFound
        raise Oasis::UnknownSwitchableDeploymentError, "Deployment not found with id #{id}"
      end
    end

    def persisted? = true

    def update(params)
      params = params.to_h.symbolize_keys
      raise Oasis::SwitchableDeploymentError.new('unsupported update') unless params.keys == [:labels]

      labels = params[:labels]
      return true if (labels.to_a - @labels.to_a).empty?

      begin
        return patch_labels(labels) && patch_pod_labels(labels)
      rescue K8s::Error::Forbidden
        raise Oasis::SwitchableDeploymentError.new 'action not allowed'
      rescue K8s::Error::NotFound
        raise Oasis::UnknownSwitchableDeploymentError.new 'object not found'
      rescue K8s::Error::MethodNotAllowed
        raise Oasis::SwitchableDeploymentError.new 'method not allowed'
      rescue K8s::Error::Invalid
        raise Oasis::SwitchableDeploymentError.new 'labels invalid'
      end
    end

    def self.create(params)
      params = params.to_h.symbolize_keys
      raise Oasis::SwitchableDeploymentError.new('unsupported update') unless params.keys == [:labels]

      param_labels = Hash[ *params[:labels].split(',').map {|e| e.split('=')}.flatten ]
      begin
        deployments = []
        watcher = K8sWatcher.instance
        deployments_to_label = watcher.get_deployments_with_labels(param_labels)
        raise Oasis::UnknownSwitchableDeploymentError.new('deployments do not exist with the selector provided') if deployments_to_label.empty?
        
        deployments_to_label.each do |deployment|
          deployment.update({
            labels: {
              Oasis::Switcher.managed_deployment_label => 'true'
            }
          })
          deployments << deployment
        end
      rescue K8s::Error::Forbidden
        raise Oasis::SwitchableDeploymentError.new 'action not allowed'
      rescue K8s::Error::NotFound
        raise Oasis::UnknownSwitchableDeploymentError.new 'object not found'
      rescue K8s::Error::MethodNotAllowed
        raise Oasis::SwitchableDeploymentError.new 'method not allowed'
      rescue K8s::Error::Invalid
        raise Oasis::SwitchableDeploymentError.new 'labels invalid'
      end
    end

    def destroy
      update({labels: {Oasis::Switcher.managed_deployment_label => nil}})
    end
  
    private

    def set_with_object(object)
      @object = object
      @name = object['metadata']['name']
      @id = @name
      @labels = object['metadata']['labels'].to_h
    end

    def patch_labels(labels)
      set_with_object(K8sWatcher.instance.patch_deployment_labels(@id, labels))
      true
    end

    def patch_pod_labels(labels)
      selector = @labels.slice(
        :'oasis.duke.edu/application-name',
        :'oasis.duke.edu/application-environment'
      )
      k8s = K8sWatcher.instance
      pods = k8s.get_pods_with_labels(selector)
      patched_pods = []
      pods.each do |pod|
        patched_pods << k8s.patch_pod_labels(pod['metadata']['name'], labels)
      end

      return patched_pods.length == pods.length
    end
  end
end
