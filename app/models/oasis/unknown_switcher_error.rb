# frozen_string_literal: true

module Oasis
  class UnknownSwitcherError < StandardError #:nodoc:
  end
end