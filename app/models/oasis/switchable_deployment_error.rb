# frozen_string_literal: true

module Oasis
  class SwitchableDeploymentError < StandardError #:nodoc:
  end
end