#there can be only one!
require 'singleton'
require 'k8s-ruby'

class K8sWatcher
  include Singleton
  attr_reader :client, :namespace

  def initialize
    begin
      @client = K8s::Client.in_cluster_config
      @namespace = File.read('/var/run/secrets/kubernetes.io/serviceaccount/namespace').chomp
    rescue K8s::Error::Configuration  
      @client = K8s::Client.config(external_configuration, persistent: true)  
    end
    @client.apis(prefetch_resources: true)
  end

  def list_routes
    @client.api('route.openshift.io/v1')
      .resource('routes', namespace: @namespace)
      .list(labelSelector: {
        'oasis.duke.edu/ingress-switcher' => 'true'
      }).map{|r| Oasis::Switcher.new(object: r) }
  end

  def find_route(id)
    raw = @client.api('route.openshift.io/v1')
      .resource('routes', namespace: @namespace)
      .get(id)
    return Oasis::Switcher.new(object: raw)
  end

  def list_managed_deployments
    @client.api('apps/v1')
      .resource('deployments', namespace: @namespace)
      .list(labelSelector: {
        'oasis.duke.edu/ingress-switcher-managed' => 'true'
      }).map{|r| Oasis::SwitchableDeployment.new(r) }
  end

  def get_deployment_managed_by(switcher_name)
    self.get_deployments_with_labels({ "oasis.duke.edu/#{switcher_name}-managed" => 'true' }).first
  end
  
  def get_deployments_with_labels(selector)
    raw = @client.api('apps/v1')
      .resource('deployments', namespace: @namespace)
      .list(labelSelector: selector)
    return [] if raw.empty?
    return raw.map {|r| Oasis::SwitchableDeployment.new(r) }
  end

  def find_deployment(id)
    raw = @client.api('apps/v1')
      .resource('deployments', namespace: @namespace)
      .get(id)
    return Oasis::SwitchableDeployment.new(raw)
  end

  def get_pods_with_labels(labels)
    @client.api('v1')
      .resource('pods', namespace: @namespace)
      .list(labelSelector: labels)
  end

  def patch_deployment_labels(name, labels)
    merge_patch = {
      metadata: {
        labels: labels
      }
    }
    patch('apps/v1','deployments',name, merge_patch)
  end

  def patch_pod_labels(name, labels)
    merge_patch = {
      metadata: {
        labels: labels
      }
    }
    patch('v1','pods',name, merge_patch)
  end

  # may raise any K8s error
  def create_service(name:, port:, labels:, selector:)
    spec = {
      apiVersion: 'v1',
      kind: 'Service',
      metadata: {
        namespace: @namespace,
        name: name,
        labels: labels
      },
      spec: {
        type: 'ClusterIP',
        ports: [
          {
            port: port.to_i,
            targetPort: port.to_i,
            name: 'http'
          },
        ],
        selector: selector
      }
    }
    return true if @client.api('v1')
      .resource('services', namespace: @namespace)
      .create_resource(
        K8s::Resource.new(spec)
      )
    return
  end

  def delete_service(name:) = delete('v1', 'services', name)

  def create_route(name:, port:, labels:, service:)
    spec = {
      apiVersion: 'route.openshift.io/v1',
      kind: 'Route',
      metadata: {
        namespace: @namespace,
        name: name,
        labels: labels
      },
      spec: {
        port: {
          targetPort: port
        },
        tls: {
          insecureEdgeTerminationPolicy: 'Redirect',
          termination: 'edge'
        },
        to: {
          kind: 'Service',
          name: service,
          weight: 100
        },
        wildcardPolicy: 'None'
      },
      status: {
        ingress: [
          {wildcardPolicy: 'None'}
        ]
      }
    }
    
    route = @client.api('route.openshift.io/v1')
      .resource('routes', namespace: @namespace)
      .create_resource(
        K8s::Resource.new(spec)
      )
    return route if route
    return
  end

  def delete_route(name:) = delete('route.openshift.io/v1', 'routes', name)

  private

  def patch(api,resource, name, patch)
    @client.api(api)
      .resource(resource, namespace: @namespace)
      .merge_patch(name, patch)
  end

  def delete(api, resource, name)
    begin
      @client.api(api)
        .resource(resource, namespace: @namespace)
        .delete(name)
      return true
    rescue K8s::Error::NotFound
      return true
    end
  end

  def external_configuration
    host = ENV['K8S_API_URL'].to_s
    raise K8sWatcherError, "K8S_API_URL required if not running in cluster" if host.empty?
    token = ENV['K8S_TOKEN'].to_s
    raise K8sWatcherError, "K8S_TOKEN required if not running in cluster" if token.empty?
    @namespace = ENV['K8S_NAMESPACE'].to_s
    raise K8sWatcherError, "K8S_NAMESPACE required if not running in cluster" if namespace.empty?

    K8s::Config.new(
      clusters: [{
          name: "#{namespace}-cluster",
          cluster: {server: host}
      }],
      users: [{
          name: "#{namespace}-user",
          user: {token: token}
      }],
      contexts: [{
          name: "#{namespace}-context",
          context: {
              cluster: "#{namespace}-cluster",
              user: "#{namespace}-user"
          }
      }],
      current_context: "#{namespace}-context"
    )
  end
end
