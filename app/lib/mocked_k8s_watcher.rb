class MockedK8sWatcher
  attr_accessor :client, :namespace, :labeled_deployments, :labeled_pods

  def initialize
    reset
  end

  def reset
    @all_deployments = {}
    @labeled_deployments = {}
    @managed_deployments = {}
    @routes = {}
    @labeled_pods = {}
  end

  def routes=(routes)
    routes.each do |route|
      @routes[route.id] = route
    end
  end
  def list_routes = @routes.values
  def find_route(id) = @routes[id]

  def deployment_managed_by(name, deployment)
    @managed_deployments[name] = deployment
  end
  def get_deployment_managed_by(switcher_name)
    @managed_deployments[switcher_name]
  end
  def list_managed_deployments = @managed_deployments.values

  def get_deployments_with_labels(selector) = @labeled_deployments[selector] || []

  def all_deployments=(deployments)
    deployments.each do |d|
      @all_deployments[d.id] = d
    end
  end
  def find_deployment(id)
    raise K8s::Error::NotFound.new('method','path','code','reason') unless @all_deployments.has_key? id

    @all_deployments[id]
  end

  def get_pods_with_labels(labels) = @labeled_pods[labels] || []

  def patch_deployment_labels(name, labels); end

  def patch_pod_labels(name, labels); end

  def create_route(name:, port:, labels:, service:); end

  def delete_route(name:); end

  def create_service(name:, port:, labels:, selector:); end

  def delete_service(name:); end

  # this may be included with Singleton
  def _dump; end
end
