# frozen_string_literal: true

class K8sWatcherError < StandardError #:nodoc:
end
