import { Controller } from "@hotwired/stimulus";
import Blockly from "blockly";

// Connects to data-controller="blockly"
export default class extends Controller {
  static targets = [
    "container",
    "switcherForm",
    "unmanagedDeployment"
  ];
  static values = {
    switchers: String,
    unmanagedDeployments: String
  }

  connect() {
    this.initializeObjects();
    this.initializeBlockly();
    this.initializeWorkspace();
  }

  initializeObjects() {
    if (typeof this.switchers === "undefined") {
      this.switchers = {};
    }
    if (typeof this.deployments === "undefined") {
      this.deployments = {};
    }
    if (typeof this.yPosition === "undefined") {
      this.yPosition = 0;
    }
  }

  initializeBlockly() {
    let ingressSwitchBlocks = this.getIngressSwitchBlocks();
    let deploymentBlocks = this.getDeploymentBlocks();
    Blockly.defineBlocksWithJsonArray(ingressSwitchBlocks.concat(deploymentBlocks));
  }

  initializeWorkspace() {
    let maxInstances = {};
    Object.keys(this.switchers).forEach((s) => {
      maxInstances['switch-'+s] = 1;
    });

    this.workspace = Blockly.inject(this.containerTarget, {
      toolbox: this.toolBoxDefinition(),
      maxInstances: maxInstances,
      maxTrashcanContents: 0
    });

    this.loadExistingIngresses();

    this.switchControl = new Blockly.Generator('IngressSwitcher');

    this.defineDeploymentBlockCodeGeneration();
    this.defineSwitchBlockCodeGeneration();

    this.workspace.addChangeListener((e) => {
      // Don't run the code when the workspace finishes loading; we're
      // already running it once when the application starts.
      // Don't run the code during drags; we might have invalid state.
      if (e.isUiEvent || e.type == Blockly.Events.FINISHED_LOADING ||
        this.workspace.isDragging()) {
        return;
      }

      if (e.type == Blockly.Events.BLOCK_DELETE) {
        if (e.oldJson.type.startsWith('switch-')){
          this.unsetSwitch(e.oldJson.type.replace('switch-',''));
        }
      }
      this.switchControl.workspaceToCode(this.workspace);
    });
  }

  toolBoxDefinition() {
    let ingressSwitchContents = Object.keys(this.switchers).map((s) => {
      return {
        kind: "block",
        type: "switch-"+s
      }
    });
  
    let deploymentContents = Object.keys(this.deployments).map((d) => {
      return {
        kind: "block",
        type: "deployment-"+d
      }
    });

    let toolbox = {
      kind: "flyoutToolbox",
      contents: ingressSwitchContents.concat(deploymentContents),
    };
    return toolbox;
  }

  switcherFormTargetConnected(element) {
    element.style.display = "none";
    this.initializeObjects();
    let s = JSON.parse(element.dataset.switcher);
    this.switchers[s.name] = s;
    if (s.managed_deployment) {
      this.deployments[s.managed_deployment.name] = s.managed_deployment;
    }
    if (this.workspace) {
      this.workspace.dispose();
      this.initializeBlockly();
      this.initializeWorkspace();
      this.loadExistingIngressFor(s);
    }
  }

  switcherFormTargetDisconnected(element) {
    delete this.switchers[element.id]
    if (this.workspace) {
      this.workspace.dispose();
      this.initializeBlockly();
      this.initializeWorkspace();
      this.loadExistingIngresses();
    }
  }

  unmanagedDeploymentTargetConnected(element) {
    this.initializeObjects();
    let d = JSON.parse(element.dataset.deployment);
    this.deployments[d.name] = d;
    if (this.workspace) {
      this.workspace.dispose();
      this.initializeBlockly();
      this.initializeWorkspace();
      this.loadExistingIngresses();
    }
  }

  unmanagedDeploymentTargetDisconnected(element) {
    delete this.deployments[element.id]
    if (this.workspace) {
      this.workspace.dispose();
      this.initializeBlockly();
      this.initializeWorkspace();
      this.loadExistingIngresses();
    }
  }

  getIngressSwitchBlocks() {
    return Object.values(this.switchers).map((switcher) => {
      return {
        type: "switch-"+switcher.name,
        message0: switcher.host+" %1",
        args0: [{
          type: "input_value",
          name: "TARGET_DEPLOYMENT"
        }],
        style: "variable_blocks",
        tooltip: "",
        helpUrl: ""
      }
    });
  }

  getDeploymentBlocks() {
   return Object.keys(this.deployments).map(this.deploymentBlock);
  }

  deploymentBlock(deployment) {
    return       {
      type: "deployment-"+deployment,
      message0: deployment+" %1",
      args0: [{
        type: "input_dummy"
      }],
      output: "String"
    }
  }

  loadExistingIngresses() {
    Object.values(this.switchers).forEach((switcher) => {
      this.loadExistingIngressFor(switcher);
    });
  }

  loadExistingIngressFor(switcher) {
    let existingIngressBlocks = this.workspace.getBlocksByType('switch-'+switcher.name);

    if(switcher.managed_deployment) {
      let newIngressBlock;
      let newDeployBlock;
      if (existingIngressBlocks.length > 0) {
        newIngressBlock = existingIngressBlocks[0];
        // console.log('using existing ingressBlock '+newIngressBlock.toDevString()+' at position '+newIngressBlock.getHeightWidth().height);
      } else {
        newIngressBlock = this.workspace.newBlock('switch-'+switcher.name);
        newIngressBlock.initSvg();
        newIngressBlock.moveBy(0, this.yPosition);
        newIngressBlock.render();
        // console.log(switcher.name+' newIngressBlock '+newIngressBlock.toDevString()+'at position '+this.yPosition);
        this.yPosition = newIngressBlock.getHeightWidth().height + 10;
      }

      let deployBlockType = 'deployment-'+switcher.managed_deployment.name;
      let childBlocks = newIngressBlock.getChildren();
      if (childBlocks.length > 0) {
        newDeployBlock = childBlocks[0];
        // console.log('using existing DeployBlock '+newDeployBlock.toDevString()+' at position '+newDeployBlock.getHeightWidth().heigth);
      } else {
        newDeployBlock = this.workspace.newBlock(deployBlockType);
        newDeployBlock.initSvg();
        newDeployBlock.render();  
        // console.log(switcher.name+' newDeployBlock '+newDeployBlock.toDevString()+'at position '+this.yPosition);
      }

      let newIngressBlockConnection = newIngressBlock.getInput('TARGET_DEPLOYMENT').connection;
      let newDeployBlockConnection = newDeployBlock.outputConnection;
      newIngressBlockConnection.connect(newDeployBlockConnection);
    } else {
      for (let i = 0; i < existingIngressBlocks.length; i++) {
        existingIngressBlocks[i].dispose(true);
      }
    }
    this.cleanup();
  }

  cleanup() {
    Object.values(this.deployments).forEach((deployment) => {
      let deployBlockType = 'deployment-'+deployment.name;
      let existingDeployBocks = this.workspace.getBlocksByType(deployBlockType);
      for(let i=0;i<existingDeployBocks.length;i++) {
        let thisBlock = existingDeployBocks[i];
        let parent = thisBlock.getParent();
        if(parent === null) {
          thisBlock.dispose(false);
        }
      }
    });
  }

  defineDeploymentBlockCodeGeneration() {
    Object.keys(this.deployments).forEach((deployment) => {
      let thatDeployment = deployment;
      this.switchControl.forBlock['deployment-'+deployment] = function(block) {
        return [thatDeployment,0];
      };
    });
  }

  defineSwitchBlockCodeGeneration() {
    Object.keys(this.switchers).forEach((switcher) => {
      let that = this;
      let thatControl = this.switchControl;
      let thatSwitchName = switcher;
      this.switchControl.forBlock['switch-'+switcher] = function(block) {
        let value = thatControl.valueToCode(block, 'TARGET_DEPLOYMENT', 0);

        if(value) {
          let code = `${thatSwitchName}: ${value}`;
          that.setSwitch(thatSwitchName, value);
          return code;
        } else {
          that.unsetSwitch(thatSwitchName);
        }
        return '';
      };
    });
  }

  unsetSwitch(name) {
    if (this.switchers[name].managed_deployment) {
      let form = document.getElementById(name);
      form.querySelector('input[type="text"]').value = "";
      form.querySelector('input[type="submit"]').click();
    }
  }

  setSwitch(name, deployment_name) {
    if (this.switchers[name].managed_deployment && this.switchers[name].managed_deployment.name === deployment_name) {
      return;
    }

    let form = document.getElementById(name);
    form.querySelector('input[type="text"]').value = deployment_name;
    form.querySelector('input[type="submit"]').click();
  }
}
