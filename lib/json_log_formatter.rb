class JsonLogFormatter < Logger::Formatter
  def call(severity, time, progname, msg)
    "{time: #{time}, severity: #{severity}, message: #{msg}}\n"
  end
end