# README

This application can be connected to a kubernetes namespace, either with 
external configuration variables, or using the in cluster configuration for
kubernetes native applications running with a service account that has sufficient RBAC.

It provides a [Blockly](https://developers.google.com/blockly) interface
that allows a 'switcher' to be selected, and then a 'deployment' to be connected to that switcher. This causes the switcher Route URL to change to serve the pods selected by the attached deployment.

## Run locally with docker-compose
1. create k8s.env using k8s.env.sample
2. connect your kubectl, k9s, etc.
3. load the example yaml objects
```
for x in examples/*
do
  k create -f $x
done
```
4. start the server and launch to http://localhost:3000 in your browser
```
bin/docker-compose/start-server.sh
```
![Initial Startup State](screenshots/initial.png "Initial Startup State")

5. Create one or more new switchers. The Port must match the containerPort for the
whoami-review deployments, 8080. If you want to make a switcher for a different
deloyment, the port must match that deployments exposed port, e.g. 3000 for a
standard rails application.

![First Switcher](screenshots/first-switcher.png "First Switcher")

If you click a link to a switcher, you will get an 'Application Not Available' screen because the switcher's Service selector does not match any existing pods.

If you `Destroy` a switcher, it will be removed from the Blockly canvas.

6. Create 'Unmanaged Deployments' for both whoami-review deployments using the
label "oasis.duke.edu/application-name=whoami". You can use commas to separate
multiple `key=value` label specifications (like using labels for kubectl get -l).

![Deployments](screenshots/deployments.png "Deployments")

7. Drag one or more switchers onto the canvas. Only one instance of each switcher
can be on the canvas at a time, so the toolbox switcher will gray out when it is
dragged onto the workspace.

![Workspace Switchers](screenshots/workspace-switchers.png "Workspace Switchers")

8. Drag one or more deployments and connect them to a switcher. You can connect the 
same deployment to multiple switchers.

![Workspace Deployments](screenshots/workspace-deployments.png "Workspace Deployments")

9. Click the link to a switcher to see that it is serving the expected whoami deployment.

![Switched Whoami](screenshots/switched-whoami.png "Switched Whoami")

Note: sometimes browser cache can mask that a url to a switcher has changed, so be sure to reload if it seems not to work.

When you click a deployment on the workspace and delete it, the switcher goes away too, and the toolbox switcher becomes available to drag onto the workspace again.

### Important Caveats
Currently this application does not have ANY authentication or authorization. It is not advised to run this in any namespace with production deployments that could be exposed without auth.

This application is Openshift specific, but plans are to make it K8s agnostic.
